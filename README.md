# QAplatform-agent

#### 介绍
质量管理平台 agent 端

#### 软件架构
软件架构说明


#### 打包 & 运行 教程

1. 进入项目目录
2. mvn 编译
    ```
    mvn clean package
    ```
3. 退出项目目录后，随便创建一个一目录
4. 将 target 目录中的 qa-platform-agent-xxxx.jar 文件复制到该目录中
5. 进入该目录后，再创建一个名为 config 的目录
6. 将 src/main/resources 目录中的 application.yml 文件复制到刚才创建的 config 目录中
7. 打开 application.yml 文件，将 spring 段删除，也就是将如下内容删除
   ```
   spring:
     version: @project.version@
     application:
       name: @project.artifactId@
     des: @project.name@
     profiles:
       active: @project.artifactId@
   ```
8. 继续修改 application.yml 文件，主要修改 qap.agent.Key、qap.server.url
   ```
   qap.agent.Key：服务器给 agent 创建的 的 key
   qap.server.url：服务器接口地址
   ```
9. 到此配置完成，完成后目录结构如下
   ```
   xxx
     config
       application.yml
     qa-platform-agent-xxxx.jar
   ```
10. 退出 config 目录，启动 jar
   ```
   java --add-opens=java.base/java.lang=ALL-UNNAMED -jar qa-platform-agent-xxxx.jar
   ```
   
#### 本地调试


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)