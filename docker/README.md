# Docker 打包

#### 介绍
如果想在 docker 或 k8s 中运行 Agent 那就需要打包成 docker 镜像

#### 打包说明

1. 进入 linux 系统，pull 下完整项目
2. 编译 java 项目
    ```
    mvn clean package
    ```
3. 将编译好的 jar 复制到 docker 目录中，并将文件名改为 qap.jar，注意根据实际情况修改版本号
    ```
    mv target/qa-platform-agent-x.x.xx.jar docker/qap.jar
    ```
4. 进入 docker 目录
   ```
   cd docker
   ```
5. 编译 docker 镜像
   ```
   docker build -t registry.cn-hangzhou.aliyuncs.com/netlrs/qa-platform-agent:v0.0.1 .
   ```
6. 推送 docker 镜像到仓库
   ```
   docker push registry.cn-hangzhou.aliyuncs.com/netlrs/qa-platform-agent:v0.0.1
   ```

   ```
   如果只是本地 build 使用则不需要修改如下内容，如果要推到远程仓库则需要根据你的仓库地址修改如下内容
   1. registry.cn-hangzhou.aliyuncs.com : 阿里云加速 docker 仓库
   2. netlrs/qa-platform-agent : 实例 + 仓库名称
   3. v0.0.1 : 镜像文件版本
   ```
