package com.bjy.qa.agent.tools.juel;

import org.apache.commons.lang.time.DateUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
class JuelHelperTest {
    /**
     * 测试自定义表达式
     */
    @Test
    public void testGetValue_01() {
        Map<String, Object> variableMap = new HashMap();

        String actual = JuelHelper.getValue(variableMap, "{\"Authorization\" : \"${auth:basic('ZhangSan', '123456')}\"}");

        String expected = "{\"Authorization\" : \"Basic WmhhbmdTYW46MTIzNDU2\"}";
        Assertions.assertEquals(expected, actual);
    }

    /**
     * 测试支持变量的自定义表达式
     */
    @Test
    public void testGetValue_02() {
        Map<String, Object> argMap = new HashMap();
        argMap.put("userName", "zs");
        argMap.put("password", "123456");

        String actual = JuelHelper.getValue(argMap, "{\"Authorization\" : \"${auth:basic(userName, password)}\"}");

        String expected = "{\"Authorization\" : \"Basic enM6MTIzNDU2\"}";
        Assertions.assertEquals(expected, actual);
    }

    /**
     * 测试变量
     */
    @Test
    public void testGetValue_03() {
        Map<String, Object> argMap = new HashMap();
        argMap.put("aa", "123");
        argMap.put("bb", "Sx@1");

        String actual = JuelHelper.getValue(argMap, "{\"aa\" : ${aa} , \"bb\" : \"${bb}\"}");

        String expected = "{\"aa\" : 123 , \"bb\" : \"Sx@1\"}";
        Assertions.assertEquals(expected, actual);
    }

    /**
     * 测试 BJY BRTC 签名
     */
    @Test
    public void testGetValue_05() {
        Map<String, Object> argMap = new HashMap();
        argMap.put("userId", "761200");
        argMap.put("password", "123456");

        String actual = JuelHelper.getValue(argMap, "{\"sign\" : \"${auth:sign_brtc('YG1O5y61cBcG0DNPRvvCXPGfd8e', 'fUdZvJPxnCN40IFDOA1vZy0mDec', userId, '115544', 1687222817, 36000)}\"}");

        String expected = "{\"sign\" : \"eJw1zN0KgjAABeB32XXIttw0oYtSGEGWlJBdmpu1zB*Wv0Xv3jC7PN85nDcIt0ejFQo4ABsQzMYsuShqmcqRLYow-DeqLHNtCBFimpM9eRZXleTazwztyUBRsk4Y9HbBoW3dKGApt8U0Fn0llQDOnEIIf1TLXAOitoUxtpE1vcqrPsysE4VeF0YFxgt68-vHalO4UL4YQWEzqLjJfdf07t3FX4LPF3GoO3c_\"}";
        Assertions.assertEquals(expected, actual);
    }

    /**
     * 测试转义
     */
    @Test
    public void testGetValue_06() {
        Map<String, Object> argMap = new HashMap();
        argMap.put("aa", "a\r\nb\nc");

        String actual = JuelHelper.getValue(argMap, "{\"aa\" : \"${param:escape(aa)}\"}");

        String expected = "{\"aa\" : \"a\\r\\nb\\nc\"}";
        Assertions.assertEquals(expected, actual);
    }

    /**
     * 测试反转义
     */
    @Test
    public void testGetValue_07() {
        Map<String, Object> argMap = new HashMap();
        argMap.put("aa", "{\"aa\" : \"a\\r\\nb\\nc\"}");

        String actual = JuelHelper.getValue(argMap, "{\"aa\" : \"${param:unescape(aa)}\"}");

        String expected = "{\"aa\" : \"{\"aa\" : \"a\r\nb\nc\"}\"}";
        Assertions.assertEquals(expected, actual);
    }

    /**
     * 测试时间戳
     */
    @Test
    public void testGetValue_08() {
        Map<String, Object> argMap = new HashMap();
        argMap.put("aa", "{\"aa\" : \"a\\r\\nb\\nc\"}");

        // ms
        String actual = JuelHelper.getValue(argMap, "${param:timestamp('')}");
        String expected = String.valueOf(System.currentTimeMillis());
        Assertions.assertEquals(expected, actual);

        // ms
        actual = JuelHelper.getValue(argMap, "${param:timestamp('ms')}");
        expected = String.valueOf(System.currentTimeMillis());
        Assertions.assertEquals(expected, actual);

        // s
        actual = JuelHelper.getValue(argMap, "${param:timestamp('s')}");
        expected = String.valueOf(System.currentTimeMillis() / 1000);
        Assertions.assertEquals(expected, actual);

        // m
        actual = JuelHelper.getValue(argMap, "${param:timestamp('m')}");
        expected = String.valueOf(System.currentTimeMillis() / 60000);
        Assertions.assertEquals(expected, actual);

        // h
        actual = JuelHelper.getValue(argMap, "${param:timestamp('h')}");
        expected = String.valueOf(System.currentTimeMillis() / 3600000);
        Assertions.assertEquals(expected, actual);

        // d
        actual = JuelHelper.getValue(argMap, "${param:timestamp('d')}");
        expected = String.valueOf(System.currentTimeMillis() / 86400000);
        Assertions.assertEquals(expected, actual);
    }

    /**
     * 测试日期
     */
    @Test
    public void testGetValue_09() {
        Map<String, Object> argMap = new HashMap();

        // 当前时间 减1天，例如 2023-06-23 23:59:59
        String actual = JuelHelper.getValue(argMap, "${param:date('-1,yyyy-MM-dd HH:mm:ss')}");
        String expected = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(DateUtils.addDays(new Date(), -1));
        Assertions.assertEquals(expected, actual);

        // 当前时间 减1天，例如 2023年06月23日 23:59:59
        actual = JuelHelper.getValue(argMap, "${param:date('-1,yyyy年MM月dd日 HH:mm:ss')}");
        expected = new SimpleDateFormat("yyyy年MM月dd日 HH:mm:ss").format(DateUtils.addDays(new Date(), -1));
        Assertions.assertEquals(expected, actual);

        // 当前时间，例如 2023-06-23 23:59:59
        actual = JuelHelper.getValue(argMap, "${param:date('0,yyyy-MM-dd HH:mm:ss')}");
        expected = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
        Assertions.assertEquals(expected, actual);

        // 当前时间 加 10 天，例如 2023-06-23 23:59:59
        actual = JuelHelper.getValue(argMap, "${param:date('10,yyyy-MM-dd HH:mm:ss')}");
        expected = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(DateUtils.addDays(new Date(), 10));
        Assertions.assertEquals(expected, actual);

        // 当前时间 加 10 天，例如 2023-06-23 23:59:59
        actual = JuelHelper.getValue(argMap, "${param:date('+10,yyyy-MM-dd HH:mm:ss')}");
        expected = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(DateUtils.addDays(new Date(), 10));
        Assertions.assertEquals(expected, actual);

        // 当前时间 加 1 年，例如 2024-06-23 23:59:59
        actual = JuelHelper.getValue(argMap, "${param:date('1,yyyy-MM-dd HH:mm:ss,y')}");
        expected = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(DateUtils.addYears(new Date(), 1));
        Assertions.assertEquals(expected, actual);

        // 当前时间的毫秒值，例如：1688602774255
        actual = JuelHelper.getValue(argMap, "${param:date('0,milliSeconds')}");
        expected = String.valueOf(System.currentTimeMillis());
        Long diff = Long.valueOf(expected) - Long.valueOf(actual);
        Assertions.assertTrue(diff <= 10); // 由于显示到毫秒，所以判断两个时间相差不到 10 就认为对

        // 当前时间， 例如：20230706140415209（年月日时分秒毫秒）
        actual = JuelHelper.getValue(argMap, "${param:date('0,yyyyMMddHHmmssSS')}");
        expected = new SimpleDateFormat("yyyyMMddHHmmssSS").format(new Date());
        diff = Long.valueOf(expected) - Long.valueOf(actual);
        Assertions.assertTrue(diff <= 10); // 由于显示到毫秒，所以判断两个时间相差不到 10 就认为对

        // 不输入 +或- 时间
        actual = JuelHelper.getValue(argMap, "${param:date(',yyyy-MM-dd HH:mm:ss')}");
        expected = "在参数：,yyyy-MM-dd HH:mm:ss 中提取出的时间格式化表达式非法: ";
        Assertions.assertEquals(expected, actual);

        // 日期表达式错误
        actual = JuelHelper.getValue(argMap, "${param:date('1,gg')}");
        expected = "在参数：1,gg 中提取出的时间格式化表达式非法: gg";
        Assertions.assertEquals(expected, actual);
    }

    /**
     * 测试字符串
     */
    @Test
    public void testGetValue_10() {
        Map<String, Object> argMap = new HashMap();

        // 随机产生 长度为5的字符串
        String actual = JuelHelper.getValue(argMap, "${param:string(5)}");
        Assertions.assertTrue(actual.length() == 5);

        // 随机产生 长度为30的字符串
        actual = JuelHelper.getValue(argMap, "${param:string(30)}");
        Assertions.assertTrue(actual.length() == 30);
    }

    /**
     * 测试特殊字符串
     */
    @Test
    public void testGetValue_11() {
        Map<String, Object> argMap = new HashMap();

        // 随机产生 长度为5的字符串
        String actual = JuelHelper.getValue(argMap, "${param:special_string(5)}");
        Assertions.assertTrue(actual.length() == 5);

        // 随机产生 长度为30的字符串
        actual = JuelHelper.getValue(argMap, "${param:special_string(30)}");
        Assertions.assertTrue(actual.length() == 30);
    }

    /**
     * 随机数
     */
    @Test
    public void testGetValue_12() {
        Map<String, Object> argMap = new HashMap();

        // 随机产生 长度为5的字符串
        String actual = JuelHelper.getValue(argMap, "${param:random(5)}");
        Assertions.assertTrue(actual.length() == 5);

        // 随机产生 长度为30的字符串
        actual = JuelHelper.getValue(argMap, "${param:random(30)}");
        Assertions.assertTrue(actual.length() == 30);
    }
}