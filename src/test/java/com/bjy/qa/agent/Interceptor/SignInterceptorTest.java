package com.bjy.qa.agent.interceptor;

import com.alibaba.fastjson.JSON;
import com.bjy.qa.agent.interceptor.tester.SignInterceptor;
import com.bjy.qa.agent.model.Step;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
class SignInterceptorTest {
    /**
     * 测试 BJY 自动签名（auto_sign_bjy 中直接写 key）
     */
    @Test
    public void testAutoSign_01() {
        String str = "{\n" +
                "   \"stepType\": 11,\n" +
                "   \"extra1\": \"{\\\"account\\\": \\\"lrs\\\",\\\"code\\\": \\\"111111\\\"}\"," +
                "   \"extra3\": \"{}\",\n" +
                "   \"extra2\": \"{\\\"room_id\\\":\\\"23052155316664\\\",\\\"user_number\\\":0,\\\"user_role\\\":\\\"0\\\",\\\"user_name\\\":\\\"技术支持\\\",\\\"user_avatar\\\":\\\"\\\",\\\"sign\\\":\\\"${auth:sign_bjy('m52+R6bsvnEgADO2+YlpswRZxe4DsKNCHO0sz5zEkVSUVuF3NTa4OMdhD99q1OBE9f+vdlSOwALx/5YMVO0F9xAdsLShMNKbTsB8K3VdxAZOCxFyVJXoRCLMP+tFYEb7')}\\\"}\",\n" +
                "   \"sort\": 2,\n" +
                "   \"delete\": false,\n" +
                "   \"extra4\": \"{}\",\n" +
                "   \"createdAt\": 1684900122000,\n" +
                "   \"errorHandlingType\": 3,\n" +
                "   \"conditionType\": 0,\n" +
                "   \"api\": {\n" +
                "       \"createdAt\": 1684900092000,\n" +
                "       \"catalogId\": 0,\n" +
                "       \"method\": \"POST\",\n" +
                "       \"name\": \"用户登录接口（login）\",\n" +
                "       \"extra1\": \"JSON\",\n" +
                "       \"id\": 4,\n" +
                "       \"sort\": 9999,\n" +
                "       \"type\": 11,\n" +
                "       \"delete\": false,\n" +
                "       \"projectId\": 1,\n" +
                "       \"url\": \"http://localhost:8080/user/user/login\",\n" +
                "       \"updatedAt\": 1684900092000\n" +
                "   },\n" +
                "   \"id\": 6148,\n" +
                "   \"projectId\": 1,\n" +
                "   \"apiId\": 4,\n" +
                "   \"testCaseId\": 6111,\n" +
                "   \"updatedAt\": 1684900122000\n" +
                "}";
        Step step = JSON.parseObject(str, Step.class);

        Map<String, Object> globalParas = new HashMap<>();

        new SignInterceptor(globalParas, step).execute();

        // 实际结果
        String actual = JSON.toJSONString(step);
        // 预期结果
        String expected = "{\"api\":{\"catalogId\":0,\"createdAt\":1684900092000,\"delete\":false,\"extra1\":\"JSON\",\"id\":4,\"method\":\"POST\",\"name\":\"用户登录接口（login）\",\"projectId\":1,\"sort\":9999,\"type\":\"INTERFACE_HTTP\",\"updatedAt\":1684900092000,\"url\":\"http://localhost:8080/user/user/login\"},\"apiId\":4,\"conditionType\":\"NONE\",\"errorHandlingType\":\"STOP\",\"extra1\":\"{\\\"account\\\": \\\"lrs\\\",\\\"code\\\": \\\"111111\\\"}\",\"extra2\":\"{\\\"room_id\\\":\\\"23052155316664\\\",\\\"user_role\\\":\\\"0\\\",\\\"user_avatar\\\":\\\"\\\",\\\"user_name\\\":\\\"技术支持\\\",\\\"sign\\\":\\\"4a5127d4afdd8e731155acdc5993ef44\\\",\\\"user_number\\\":0}\",\"extra3\":\"{}\",\"extra4\":\"{}\",\"projectId\":1,\"sort\":2,\"stepType\":\"INTERFACE_HTTP\",\"testCaseId\":6111}";

        Assertions.assertEquals(expected, actual);
    }

    /**
     * 测试 BJY 自动签名（auto_sign_bjy 中直接 key 引用一个变量）
     */
    @Test
    public void testAutoSign_02() {
        String str = "{\n" +
                "   \"stepType\": 11,\n" +
                "   \"extra1\": \"{\\\"account\\\": \\\"lrs\\\",\\\"code\\\": \\\"111111\\\"}\"," +
                "   \"extra3\": \"{}\",\n" +
                "   \"extra2\": \"{\\\"room_id\\\":\\\"23052155316664\\\",\\\"user_number\\\":0,\\\"user_role\\\":\\\"0\\\",\\\"user_name\\\":\\\"技术支持\\\",\\\"user_avatar\\\":\\\"\\\",\\\"sign\\\":\\\"${auth:sign_bjy(key)}\\\"}\",\n" +
                "   \"sort\": 2,\n" +
                "   \"delete\": false,\n" +
                "   \"extra4\": \"{}\",\n" +
                "   \"createdAt\": 1684900122000,\n" +
                "   \"errorHandlingType\": 3,\n" +
                "   \"conditionType\": 0,\n" +
                "   \"api\": {\n" +
                "       \"createdAt\": 1684900092000,\n" +
                "       \"catalogId\": 0,\n" +
                "       \"method\": \"POST\",\n" +
                "       \"name\": \"用户登录接口（login）\",\n" +
                "       \"extra1\": \"JSON\",\n" +
                "       \"id\": 4,\n" +
                "       \"sort\": 9999,\n" +
                "       \"type\": 11,\n" +
                "       \"delete\": false,\n" +
                "       \"projectId\": 1,\n" +
                "       \"url\": \"http://localhost:8080/user/user/login\",\n" +
                "       \"updatedAt\": 1684900092000\n" +
                "   },\n" +
                "   \"id\": 6148,\n" +
                "   \"projectId\": 1,\n" +
                "   \"apiId\": 4,\n" +
                "   \"testCaseId\": 6111,\n" +
                "   \"updatedAt\": 1684900122000\n" +
                "}";
        Step step = JSON.parseObject(str, Step.class);

        Map<String, Object> globalParas = new HashMap<>();
        globalParas.put("key", "m52+R6bsvnEgADO2+YlpswRZxe4DsKNCHO0sz5zEkVSUVuF3NTa4OMdhD99q1OBE9f+vdlSOwALx/5YMVO0F9xAdsLShMNKbTsB8K3VdxAZOCxFyVJXoRCLMP+tFYEb7");

        new SignInterceptor(globalParas, step).execute();

        // 实际结果
        String actual = JSON.toJSONString(step);
        // 预期结果
        String expected = "{\"api\":{\"catalogId\":0,\"createdAt\":1684900092000,\"delete\":false,\"extra1\":\"JSON\",\"id\":4,\"method\":\"POST\",\"name\":\"用户登录接口（login）\",\"projectId\":1,\"sort\":9999,\"type\":\"INTERFACE_HTTP\",\"updatedAt\":1684900092000,\"url\":\"http://localhost:8080/user/user/login\"},\"apiId\":4,\"conditionType\":\"NONE\",\"errorHandlingType\":\"STOP\",\"extra1\":\"{\\\"account\\\": \\\"lrs\\\",\\\"code\\\": \\\"111111\\\"}\",\"extra2\":\"{\\\"room_id\\\":\\\"23052155316664\\\",\\\"user_role\\\":\\\"0\\\",\\\"user_avatar\\\":\\\"\\\",\\\"user_name\\\":\\\"技术支持\\\",\\\"sign\\\":\\\"945ad6b6b860cc86fc2b8341d7e3cd27\\\",\\\"user_number\\\":0}\",\"extra3\":\"{}\",\"extra4\":\"{}\",\"projectId\":1,\"sort\":2,\"stepType\":\"INTERFACE_HTTP\",\"testCaseId\":6111}";

        Assertions.assertEquals(expected, actual);
    }

    /**
     * 测试 BJY 自动签名（参数中有 json 字符数组）
     */
    @Test
    public void testAutoSign_03() {
        String str = "{\n" +
                "                \"api\": {\n" +
                "                    \"catalogId\": 886,\n" +
                "                    \"createdAt\": 1688350330000,\n" +
                "                    \"delete\": false,\n" +
                "                    \"extra1\": \"FORM\",\n" +
                "                    \"id\": 108,\n" +
                "                    \"method\": \"POST\",\n" +
                "                    \"name\": \"发送聊天消息接口\",\n" +
                "                    \"projectId\": 14,\n" +
                "                    \"sort\": 43,\n" +
                "                    \"type\": 11,\n" +
                "                    \"updatedAt\": 1688976992000,\n" +
                "                    \"url\": \"https://e66719403.test-at.baijiayun.com/openapi/live/sendChatMessage\"\n" +
                "                },\n" +
                "                \"apiId\": 108,\n" +
                "                \"conditionType\": 0,\n" +
                "                \"createdAt\": 1688367753000,\n" +
                "                \"delete\": false,\n" +
                "                \"errorHandlingType\": 3,\n" +
                "                \"extra1\": \"{}\",\n" +
                "                \"extra2\": \"{}\",\n" +
                "                \"extra3\": \"{\\n  \\\"partner_id\\\": \\\"66719403\\\",\\n  \\\"message_list\\\":\\\"[         {             \\\\\\\"message_type\\\\\\\": \\\\\\\"message_send\\\\\\\",             \\\\\\\"content\\\\\\\": \\\\\\\"学生api发的第1条消息\\\\\\\",             \\\\\\\"to\\\\\\\": \\\\\\\"-1\\\\\\\",             \\\\\\\"from\\\\\\\": {                 \\\\\\\"id\\\\\\\": \\\\\\\"1548760\\\\\\\",                 \\\\\\\"number\\\\\\\": \\\\\\\"438964741\\\\\\\",                 \\\\\\\"type\\\\\\\": 0,                 \\\\\\\"name\\\\\\\": \\\\\\\"test\\\\\\\",                 \\\\\\\"avatar\\\\\\\": \\\\\\\"https://test-img.baijiayun.com/0bjcloud/live/avatar/v2/90.jpg\\\\\\\",                 \\\\\\\"status\\\\\\\": 0,                 \\\\\\\"end_type\\\\\\\": 0,                 \\\\\\\"group\\\\\\\": 0,                 \\\\\\\"webrtc_support\\\\\\\": 1             },             \\\\\\\"uid\\\\\\\": \\\\\\\"1548760\\\\\\\",             \\\\\\\"agent_id\\\\\\\": 0,             \\\\\\\"time\\\\\\\": 1553658174,             \\\\\\\"id\\\\\\\": \\\\\\\"1\\\\\\\",             \\\\\\\"class_id\\\\\\\": \\\\\\\"23070343896474\\\\\\\",             \\\\\\\"group\\\\\\\": 0         }     ]\\\",\\n  \\\"timestamp\\\": \\\"1688971022\\\",\\n\\t\\\"sign\\\": \\\"${auth:sign_bjy(partner_key)}\\\"\\n}\",\n" +
                "                \"extra4\": \"{}\",\n" +
                "                \"extra5\": \"{\\n  \\\"code\\\": 0\\n}\",\n" +
                "                \"id\": 8412,\n" +
                "                \"projectId\": 14,\n" +
                "                \"sort\": 1,\n" +
                "                \"stepType\": 11,\n" +
                "                \"testCaseId\": 7905,\n" +
                "                \"updatedAt\": 1688367753000\n" +
                "            }";
        Step step = JSON.parseObject(str, Step.class);

        Map<String, Object> globalParas = new HashMap<>();
        globalParas.put("partner_id","66719403");
        globalParas.put("partner_key","oJVpUngvkUPnTqhqZNub1o/1YpYr2UIjUDRxhZN2TkZSeUntuJwxTGS1Qsz2WJtstbZ8WI2Jf4lccDvjAS6w7FLei5Aw");
        globalParas.put("time" , "8993721509");
        globalParas.put("api_base_url","https://e66719403.test-at.baijiayun.com/");

        new SignInterceptor(globalParas, step).execute();

        // 实际结果
        String actual = JSON.toJSONString(step);
        // 预期结果
        String expected = "{\"api\":{\"catalogId\":886,\"createdAt\":1688350330000,\"delete\":false,\"extra1\":\"FORM\",\"id\":108,\"method\":\"POST\",\"name\":\"发送聊天消息接口\",\"projectId\":14,\"sort\":43,\"type\":\"INTERFACE_HTTP\",\"updatedAt\":1688976992000,\"url\":\"https://e66719403.test-at.baijiayun.com/openapi/live/sendChatMessage\"},\"apiId\":108,\"conditionType\":\"NONE\",\"errorHandlingType\":\"STOP\",\"extra1\":\"{}\",\"extra2\":\"{}\",\"extra3\":\"{\\\"message_list\\\":\\\"[         {             \\\\\\\"message_type\\\\\\\": \\\\\\\"message_send\\\\\\\",             \\\\\\\"content\\\\\\\": \\\\\\\"学生api发的第1条消息\\\\\\\",             \\\\\\\"to\\\\\\\": \\\\\\\"-1\\\\\\\",             \\\\\\\"from\\\\\\\": {                 \\\\\\\"id\\\\\\\": \\\\\\\"1548760\\\\\\\",                 \\\\\\\"number\\\\\\\": \\\\\\\"438964741\\\\\\\",                 \\\\\\\"type\\\\\\\": 0,                 \\\\\\\"name\\\\\\\": \\\\\\\"test\\\\\\\",                 \\\\\\\"avatar\\\\\\\": \\\\\\\"https://test-img.baijiayun.com/0bjcloud/live/avatar/v2/90.jpg\\\\\\\",                 \\\\\\\"status\\\\\\\": 0,                 \\\\\\\"end_type\\\\\\\": 0,                 \\\\\\\"group\\\\\\\": 0,                 \\\\\\\"webrtc_support\\\\\\\": 1             },             \\\\\\\"uid\\\\\\\": \\\\\\\"1548760\\\\\\\",             \\\\\\\"agent_id\\\\\\\": 0,             \\\\\\\"time\\\\\\\": 1553658174,             \\\\\\\"id\\\\\\\": \\\\\\\"1\\\\\\\",             \\\\\\\"class_id\\\\\\\": \\\\\\\"23070343896474\\\\\\\",             \\\\\\\"group\\\\\\\": 0         }     ]\\\",\\\"partner_id\\\":\\\"66719403\\\",\\\"sign\\\":\\\"c354d0d073177525dd47e76d27aef8ff\\\",\\\"timestamp\\\":\\\"1688971022\\\"}\",\"extra4\":\"{}\",\"extra5\":\"{\\n  \\\"code\\\": 0\\n}\",\"projectId\":14,\"sort\":1,\"stepType\":\"INTERFACE_HTTP\",\"testCaseId\":7905}";

        Assertions.assertEquals(expected, actual);
    }
}