package com.bjy.qa.agent.enumtype;

import java.io.Serializable;

/**
 * 条件 枚举类（实现）
 */
public enum ConditionEnum implements IConditionEnum<Integer>, Serializable {
    NONE("none", 0), // 不是 条件步骤
    IF("if", 1), // if 条件步骤
    ELSE_IF("else_if", 2), //else if 条件步骤
    ELSE("else", 3), // else 条件步骤
    WHILE("while", 4), // while 条件步骤
    FOR("for", 5); // for 条件步骤

    private final Integer value;

    private final String name;

    ConditionEnum(String name, int value) {
        this.name = name;
        this.value = value;
    }

    @Override
    public Integer getValue() {
        return value;
    }

    public String getName() {
        return name;
    }
}
