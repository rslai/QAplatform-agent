package com.bjy.qa.agent.enumtype;

/**
 * 运行模式 枚举类
 */
public enum RunningModeStatus {
    DEBUGGING("DEBUGGING", 1),
    TESTING("TESTING", 2);

    private String name; // 运行模式名称
    private int value; // 运行模式值

    RunningModeStatus(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }
}
