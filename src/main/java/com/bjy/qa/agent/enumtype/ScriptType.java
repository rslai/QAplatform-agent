package com.bjy.qa.agent.enumtype;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * 性能测试脚本类型 枚举类
 */
public enum ScriptType {
    LOCUST("Locust", 1),
    JMETER("JMeter", 2),
    LOAD_RUNNER("LoadRunner", 3);

    private String name;

    @JsonValue
    private int value;

    ScriptType(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    @JSONField
    public int getValue() {
        return value;
    }
}
