package com.bjy.qa.agent.enumtype;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * 分类类型 枚举类
 */
public enum CatalogType {
    NONE("none", 0),
    TEST_CASE("测试用例", 1),
    INTERFACE_TEST_CASE("接口测试用例", 3),
    PUBLIC_STEP("公共步骤", 4),
    TEST_SUITE("测试套件", 2),
    PERFORMANCE_TEST_SCRIPT("性能测试用例", 5),
    PERFORMANCE_TEST_SUITE("性能测试套件", 6),
    INTERFACE_HTTP("http 接口", 11),
    INTERFACE_WEB_SOCKET("WebSocket 接口", 12),
    INTERFACE_DATA_CHANNEL("DataChannel 接口", 13),
    INTERFACE_DATABASE("Database 接口", 14),
    INTERFACE_SSH("SSH 接口", 15),
    METHOD_STEP_HOLD("全局步骤间隔", 101),
    METHOD_ASSERT("断言", 103),
    METHOD_WAIT("等待", 105),
    METHOD_WEB_SOCKET_WAIT("WebSocket 等待", 106),
    RUN_SCRIPT("自定义脚本", 107),
    METHOD_SAVE_PARAS("保存变量", 104),
    METHOD_SET_PARAS("设置变量", 108);

    private String name;

    @JsonValue
    private int value;

    CatalogType(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }
}
