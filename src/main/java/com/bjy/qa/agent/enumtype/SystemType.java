package com.bjy.qa.agent.enumtype;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * 操作系统类型 枚举类
 */
public enum SystemType {
    LINUX("linux", 1),
    WINDOWS("windows", 2),
    MAC("mac", 3);

    private String name;

    @JsonValue
    private int value;

    SystemType(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    @JSONField
    public int getValue() {
        return value;
    }
}
