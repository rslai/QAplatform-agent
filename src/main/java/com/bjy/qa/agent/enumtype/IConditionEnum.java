package com.bjy.qa.agent.enumtype;

import org.springframework.util.Assert;

import java.util.stream.Stream;

/**
 * 条件 枚举类（接口）
 */
public interface IConditionEnum<T> {

    /**
     * 获取枚举值<T>
     *
     * @return enum value
     */
    T getValue();

    /**
     * 将 value 转成枚举
     */
    static <T, E extends Enum<E> & IConditionEnum<T>> E valueToEnum(Class<E> enumType, T value) {
        Assert.notNull(enumType, "enum type must not be null");
        Assert.notNull(value, "value must not be null");
        Assert.isTrue(enumType.isEnum(), "type must be an enum type");

        return Stream.of(enumType.getEnumConstants())
                .filter(item -> item.getValue().equals(value))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("unknown database value: " + value));
    }

}
