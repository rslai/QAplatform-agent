package com.bjy.qa.agent.enumtype;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * 性能测试日志类型 枚举类
 */
public enum PerfLogType {
    UNKNOWN("未知", 0),
    LOG_REQUESTS("/stats/requests", 10),
    LOG_TASKS("/tasks", 11),
    LOG_EXCEPTIONS("/exceptions", 12),
    CSV_REQUESTS("/stats/requests/csv", 20),
    CSV_FAILURES("/stats/failures/csv", 21),
    CSV_EXCEPTIONS("/exceptions/csv", 22),
    REPORT("性能测试报告页签 - 表格中显示的数据", 30),
    LATEST_DATA("性能测试实时图表页签 - 表格中显示的数据", 31),
    CHART("性能测试实时图表页签 - chart 绘图需要的数据", 32);

    private String name;

    @JsonValue
    private int value;

    PerfLogType(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    @JSONField
    public int getValue() {
        return value;
    }
}
