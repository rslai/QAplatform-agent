package com.bjy.qa.agent.enumtype;

/**
 * 错误处理类型 枚举类
 */
public enum ErrorHandlingType {
    IGNORE("忽略", 1),
    WARNING("警告", 2),
    STOP("中断", 3);

    private String name; // 状态名称
    private int value; // 状态值

    ErrorHandlingType(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }
}
