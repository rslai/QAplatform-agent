package com.bjy.qa.agent.transport.websocket;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * WebSocket 配置信息
 */
@Component
public class WebSocketConf {
    // 是否开启 debug 模式
    @Value("${qap.websocket.debug:false}")
    private Boolean debug;

    public Boolean getDebug() {
        return debug;
    }

    public void setDebug(Boolean debug) {
        this.debug = debug;
    }

    @Override
    public String toString() {
        return "WebSocketConf{" +
                "debug=" + debug +
                '}';
    }
}
