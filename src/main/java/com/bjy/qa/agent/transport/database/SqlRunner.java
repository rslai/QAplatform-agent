package com.bjy.qa.agent.transport.database;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.*;
import java.util.*;

/**
 * sql 执行器
 */
public class SqlRunner {
    private static final Logger logger = LoggerFactory.getLogger(SqlRunner.class);

    private DataSource dataSource; // 数据源

    /**
     * 构造函数
     * @param driverClass 数据库驱动
     * @param url 数据库连接字串
     * @param username 数据库用户名
     * @param password 数据库密码
     */
    public SqlRunner(String driverClass, String url, String username, String password) {
        this.dataSource = new PooledDataSource(driverClass, url, username, password);
    }

    /**
     * 执行 sql
     * @param sql sql 语句
     * @return
     * @throws Exception
     */
    public List<Map<String, Object>> execute(String sql) throws Exception {
        Connection connection = null;
        try {
            connection = this.dataSource.getConnection();
            return execute(connection, sql);
        } finally {
            close(connection);
        }
    }

    /**
     * 执行 sql
     * @param connection 数据库连接
     * @param sql sql 语句
     * @return
     * @throws SQLException
     */
    private List<Map<String, Object>> execute(Connection connection, String sql) throws SQLException {
        sql = sql.trim();
        if (isQuery(sql)) {
            return query(connection, sql);
        }
        return executeBatch(connection, sql);
    }

    /**
     * 执行批量 sql
     * @param connection 数据库连接
     * @param sql sql 语句
     * @return
     * @throws SQLException
     */
    private List executeBatch(Connection connection, String sql) throws SQLException {
        Statement statement = null;
        try {
            statement = connection.createStatement();
            String[] sqlList = StringUtils.split(sql, ";");
            for (String singleSql : sqlList) {
                statement.addBatch(singleSql);
            }
            Map temp = new HashMap();
            temp.put("result", statement.executeBatch());
            return Arrays.asList(new Map[]{temp});
        } finally {
            close(statement);
        }
    }

    /**
     * 查询 sql
     * @param connection 数据库连接
     * @param sql sql 语句
     * @return
     * @throws SQLException
     */
    private List<Map<String, Object>> query(Connection connection, String sql) throws SQLException {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(sql);
            List result = new ArrayList();
            ResultSet resultSet = statement.executeQuery();
            int columnCount = resultSet.getMetaData().getColumnCount();
            while (resultSet.next()) {
                result.add(extractRow(resultSet, columnCount));
            }
            close(resultSet);
            return result;
        } finally {
            close(statement);
        }
    }

    /**
     * 判断是否是查询语句
     * @param sql sql 语句
     * @return
     */
    private boolean isQuery(String sql) {
        return sql.toLowerCase().startsWith("select");
    }

    /**
     * 提取查询结果
     * @param resultSet 查询结果
     * @param columnCount 列数
     * @return
     * @throws SQLException
     */
    private Map<String, Object> extractRow(ResultSet resultSet, int columnCount) throws SQLException {
        Map result = new HashMap();
        for (int i = 1; i <= columnCount; i++) {
            result.put(resultSet.getMetaData().getColumnLabel(i), resultSet.getString(i));
        }
        return result;
    }

    /**
     * 关闭 ResultSet
     * @param resultSet 查询结果
     */
    private void close(ResultSet resultSet) {
        if (resultSet != null)
            try {
                resultSet.close();
            } catch (SQLException e) {
                logger.error("ResultSet close error", e);
            }
    }

    /**
     * 关闭数据库连接
     * @param connection 数据库连接
     */
    private void close(Connection connection) {
        if (connection != null)
            try {
                connection.close();
            } catch (SQLException e) {
                logger.error("Connection close error", e);
            }
    }

    /**
     * 关闭 Statement
     * @param statement Statement
     */
    private void close(Statement statement) {
        if (statement == null) return;
        try {
            statement.close();
        } catch (SQLException e) {
            logger.error(e.getMessage(), e);
        }
    }
}
