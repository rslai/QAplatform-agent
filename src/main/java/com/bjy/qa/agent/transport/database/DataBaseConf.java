package com.bjy.qa.agent.transport.database;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * DataBase 配置信息
 */
@Component
public class DataBaseConf {
    // 是否开启 debug 模式
    @Value("${qap.database.debug:false}")
    private Boolean debug;

    public Boolean getDebug() {
        return debug;
    }

    public void setDebug(Boolean debug) {
        this.debug = debug;
    }

    @Override
    public String toString() {
        return "DataBaseConf{" +
                "debug=" + debug +
                '}';
    }
}
