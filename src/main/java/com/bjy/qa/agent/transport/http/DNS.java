package com.bjy.qa.agent.transport.http;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 本地 DNS 解析工具类
 */
public class DNS {
    private static final Logger logger = LoggerFactory.getLogger(DNS.class);

    private static final Map<String, String> DOMAIN2IP = new HashMap();
    private static final String DEFAULT_PROTOCOL_PREFIX = "http://";

    /**
     * 读取本地 hosts 中配置的域名解析数据，填充到 DOMAIN2IP 中
     */
    private static void loadCustomHosts() {
        InputStream stream = HttpService.class.getClassLoader().getResourceAsStream("hosts");
        if (stream == null) return;
        try {
            List<String> lines = IOUtils.readLines(stream);
            for (String line : lines)
                if (!isComment(line)) {
                    String[] items = StringUtils.split(line, " ");
                    if (items.length >= 2)
                        for (int i = 1; i < items.length; i++)
                            if (!isComment(items[i]))
                                DOMAIN2IP.put(StringUtils.trim(items[i]), StringUtils.trim(items[0]));
                }
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        } finally {
            IOUtils.closeQuietly(stream);
        }
    }

    /**
     * 判断是否是注释行（以 # 开头的行）
     * @param line 行内容
     * @return
     */
    private static boolean isComment(String line) {
        return StringUtils.startsWith(StringUtils.trim(line), "#");
    }

    /**
     * DNS 解析，将域名解析为 IP 地址
     * @param domain 域名
     * @return IP 地址
     */
    public static String dnsLookup(String domain) {
        String ip = (String) DOMAIN2IP.get(domain);
        if (StringUtils.isBlank(ip)) return domain;
        return ip;
    }

    /**
     * 获取域名的主机名（后续会填到 http head 的 Host 中）
     * 例：http://www.baidu.com/abc/def?param=1 返回 www.baidu.com
     * @param uri uri
     * @return 主机名
     */
    public static String getHost(String uri) {
        URL url = null;
        try {
            if (!StringUtils.startsWith(uri, "http://") && !StringUtils.startsWith(uri, "https://")) {
                uri = "http://" + uri;
            }
            url = new URL(uri);
        } catch (MalformedURLException e) {
            return null;
        }
        return url.getHost();
    }

    static {
        loadCustomHosts();
    }

}
