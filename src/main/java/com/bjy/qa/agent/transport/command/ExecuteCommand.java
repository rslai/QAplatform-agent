package com.bjy.qa.agent.transport.command;


import com.bjy.qa.agent.model.KeyValueStore;
import com.bjy.qa.agent.response.Response;

import java.util.List;

/**
 * 执行命令抽象类
 */
public abstract class ExecuteCommand {
    protected String resultId; // 运行结果 id
    protected String caseId; // 用例 id
    protected String ic; // ic: 迭代次数（iteration count）
    protected String desc; // 用例描述

    /**
     * 构造函数
     * @param resultId 运行结果 id
     * @param caseId 用例 id
     * @param desc 用例描述
     */
    public ExecuteCommand(String resultId, String caseId, String ic, String desc) {
        this.resultId = resultId;
        this.caseId = caseId;
        this.ic = ic;
        this.desc = desc;
    }

    public String getResultId() {
        return resultId;
    }

    public String getCaseId() {
        return caseId;
    }

    public String getDesc() {
        return this.desc;
    }

    /**
     * 执行命令（抽象方法），每个子类都需要实现，也是主功能
     * @param paramList 参数列表
     * @return 自定义的 HttpResponse
     */
    public abstract Response execute(List<KeyValueStore> paramList);

    /**
     * 获得测试报告（测试结果上报使用）
     * @return
     */
    public abstract String toReportInfo();
}
