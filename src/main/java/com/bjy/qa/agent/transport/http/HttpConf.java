package com.bjy.qa.agent.transport.http;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * httpclient 配置信息
 */
@Component
public class HttpConf {
    // 是否开启 debug 模式
    @Value("${qap.http.debug:false}")
    private Boolean debug;

    // http 版本
    @Value("${qap.http.version:1.1}")
    private String version;

    // 是否开启自动处理重定向
    @Value("${qap.http.protocol-handle-redirects:true}")
    private Boolean protocolHandleRedirects;

    // http 建立连接的超时时间
    @Value("${qap.http.connection-timeout:10000}")
    private Integer connectionTimeout;

    // http 创建连接成功后，读取数据的超时时间
    @Value("${qap.http.socket-timeout:600000}")
    private Integer socketTimeout;

    // 是否开启使用旧连接检查。开启检查会降低性能但能保证不会出现 IOExceptions
    @Value("${qap.http.connection-stalecheck:true}")
    private Boolean connectionStalecheck;

    // 最大总连接数
    @Value("${qap.http.max-total-connections:200}")
    private Integer maxTotalConnections;

    // 单路由最大连接数（单路由指的是指向同一目标主机和端口的连接）
    @Value("${qap.http.max-per-route:50}")
    private Integer maxPerRoute;

    // 是否开启追踪，开启后会在请求头中添加 x-qap-request-id
    @Value("${qap.http.tracking-markers:false}")
    private Boolean trackingMarkers;

    public Boolean getDebug() {
        return debug;
    }

    public void setDebug(Boolean debug) {
        this.debug = debug;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Boolean getProtocolHandleRedirects() {
        return protocolHandleRedirects;
    }

    public void setProtocolHandleRedirects(Boolean protocolHandleRedirects) {
        this.protocolHandleRedirects = protocolHandleRedirects;
    }

    public Integer getConnectionTimeout() {
        return connectionTimeout;
    }

    public void setConnectionTimeout(Integer connectionTimeout) {
        this.connectionTimeout = connectionTimeout;
    }

    public Integer getSocketTimeout() {
        return socketTimeout;
    }

    public void setSocketTimeout(Integer socketTimeout) {
        this.socketTimeout = socketTimeout;
    }

    public Boolean getConnectionStalecheck() {
        return connectionStalecheck;
    }

    public void setConnectionStalecheck(Boolean connectionStalecheck) {
        this.connectionStalecheck = connectionStalecheck;
    }

    public Integer getMaxTotalConnections() {
        return maxTotalConnections;
    }

    public void setMaxTotalConnections(Integer maxTotalConnections) {
        this.maxTotalConnections = maxTotalConnections;
    }

    public Integer getMaxPerRoute() {
        return maxPerRoute;
    }

    public void setMaxPerRoute(Integer maxPerRoute) {
        this.maxPerRoute = maxPerRoute;
    }

    public Boolean getTrackingMarkers() {
        return trackingMarkers;
    }

    public void setTrackingMarkers(Boolean trackingMarkers) {
        this.trackingMarkers = trackingMarkers;
    }

    @Override
    public String toString() {
        return "HttpClientConf{" +
                "debug=" + debug +
                ", version='" + version + '\'' +
                ", protocolHandleRedirects=" + protocolHandleRedirects +
                ", connectionTimeout=" + connectionTimeout +
                ", socketTimeout=" + socketTimeout +
                ", connectionStalecheck=" + connectionStalecheck +
                ", maxTotalConnections=" + maxTotalConnections +
                ", maxPerRoute=" + maxPerRoute +
                ", trackingMarkers=" + trackingMarkers +
                '}';
    }
}
