package com.bjy.qa.agent.transport.websocket;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * 报文结尾类型 枚举类
 */
public enum EndingType {
    NONE("none", 0),
    SINGLE_PACK("单包报文，收到数据即认为完成（不处理分包问题）", 1),
    ENDING_SYMBOL("包尾含特殊字符（文本报文）", 2),
    HEAD_BODY("[包头+包体]格式报文（包头存包体字节长度）", 3),
    FIXED_LENGTH("定长报文", 4);

    private String name;

    @JsonValue
    private int value;

    EndingType(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }
}
