package com.bjy.qa.agent.transport.websocket;

import com.alibaba.fastjson.JSONObject;

/**
 * WebSocket 服务接口
 */
public interface IWebSocketService {
    /**
     * 发送消息
     * @param sendText 待发送的消息内容
     * @param interval 发送间隔
     * @param assertStr assert 断言
     * @param setStr 保存变量
     * @param lose 是否丢弃
     * @param options 扩展数据（通道、服务器是否要关闭连接）
     */
    public void send(String sendText, long interval, String assertStr, String setStr, boolean lose, JSONObject options);

    /**
     * 接收消息
     * @param sendText 待发送的消息内容（当收到服务器 push 消息后，需要响应，所发送的内容）
     * @param interval 发送间隔
     * @param assertStr assert 断言
     * @param setStr 保存变量
     * @param lose 是否丢弃
     * @param options 扩展数据（通道、服务器是否要关闭连接）
     */
    public void receive(String sendText, long interval, String assertStr, String setStr, boolean lose, JSONObject options);

    /**
     * 关闭连接
     */
    public void close(String msg);

    /**
     * 是否有待处理的 pending 项
     * 消息列表中，有待处理的 pending 项，返回 true，否则返回 false
     * @return
     */
    public boolean hasPending();

    /**
     * 得到待处理的 pending 信息
     * @return
     */
    public String getPendingMessages();
}