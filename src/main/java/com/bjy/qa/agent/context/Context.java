package com.bjy.qa.agent.context;

import com.bjy.qa.agent.transport.http.HttpService;
import com.bjy.qa.agent.transport.websocket.IWebSocketService;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 上下文, 保存变量、参数、环境
 */
public class Context {
    private int defaultTime = 6; // 默认生命值
    private int time = defaultTime; // 生命值，当 time=0 时会清除此 Context
    private HttpService httpService; // httpService（持有 RslaiHttpClient）
    private Map<String, IWebSocketService> iWebSocketServiceMap = new ConcurrentHashMap(); // webSocketServiceMap（持有 WebSocketService）
    private Map<String, Object> globalParas = new ConcurrentHashMap(); // 全局参数
    public final static String WEB_SOCKET_SERVICE_KEY = "%s::rid:%s-cid:%s-channel:%s"; // WebSocketService key

    /**
     * 得到 生命值
     * @return
     */
    public int getTime() {
        return time--;
    }

    /**
     * 得到 HttpService
     * @return
     */
    public HttpService getHttpService() {
        time = defaultTime; // 恢复生命值
        if (httpService == null) {
            httpService = new HttpService();
        }
        return httpService;
    }

    /**
     * 设置 HttpService
     * @param httpService
     */
    public void setHttpService(HttpService httpService) {
        time = defaultTime; // 恢复生命值
        this.httpService = httpService;
    }

    /**
     * 得到一个 WebSocketService
     * @param url WebSocket url
     * @param resultId 结果 id
     * @param caseId 用例 id
     * @param channel 通道
     * @return
     */
    public IWebSocketService getWebSocketService(String url, String resultId, String caseId, String channel) {
        return iWebSocketServiceMap.get(String.format(WEB_SOCKET_SERVICE_KEY, url, resultId, caseId, channel));
    }

    /**
     * 添加一个 WebSocketService
     * @param url WebSocket url
     * @param resultId 结果 id
     * @param caseId 用例 id
     * @param channel 通道
     * @param iWebSocketService WebSocketService
     */
    public void addWebSocketService(String url, String resultId, String caseId, String channel, IWebSocketService iWebSocketService) {
        this.iWebSocketServiceMap.put(String.format(WEB_SOCKET_SERVICE_KEY, url, resultId, caseId, channel), iWebSocketService);
    }

    public Map<String, IWebSocketService> getWebSocketServiceMap() {
        return iWebSocketServiceMap;
    }

    public void setWebSocketServiceMap(Map<String, IWebSocketService> iWebSocketServiceMap) {
        this.iWebSocketServiceMap = iWebSocketServiceMap;
    }

    /**
     * 得到 全局参数
     * @return
     */
    public Map<String, Object> getGlobalParas() {
        time = defaultTime; // 恢复生命值
        return globalParas;
    }

    /**
     * 设置 全局参数
     * @param globalParas
     */
    public void setGlobalParas(Map<String, Object> globalParas) {
        time = defaultTime; // 恢复生命值
        this.globalParas = globalParas;
    }

    /**
     * 向 全局参数 里添加数据，如果已有相同 key 会被替换
     * @param key
     * @param value
     */
    public void addGlobalParas(String key, Object value) {
        time = defaultTime; // 恢复生命值
        this.globalParas.put(key, value);
    }
}
