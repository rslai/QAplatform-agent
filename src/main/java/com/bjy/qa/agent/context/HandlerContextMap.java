package com.bjy.qa.agent.context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 套件运行（runSuite）中持有 Context
 */
@Configuration
@EnableScheduling
public class HandlerContextMap {
    private static final Logger logger = LoggerFactory.getLogger(HandlerContextMap.class);

    private static Map<String, Context> contexts = new ConcurrentHashMap<>();

    /**
     * 防止内存溢出，定时清理没用的 Context
     */
    @Scheduled(fixedDelay = 5 * 60 * 1000)
    public void cleaningContextMap() {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String, Context> entry : HandlerContextMap.contexts.entrySet()) {
            String key = entry.getKey();
            Context context = entry.getValue();
            int time = context.getTime();
            if (time == 0 ) {
                HandlerContextMap.removeContext(key);
            }
            sb.append("{key:" + key + ", time:" + time + "},");
        }
        logger.info("HandlerContextMap [{}]", sb.toString());
    }

    /**
     * 添加一个 Context
     * @param resultId resultId（唯一ID key）
     * @return
     */
    public static void addContext(String resultId, Context context) {
        contexts.put(resultId, context);
    }

    /**
     * 添加一个 Context
     * @param resultId resultId（唯一ID key）
     * @return
     */
    public static void addContext(int resultId, Context context) {
        addContext(String.valueOf(resultId), context);
    }

    /**
     * 根据 resultId 得到一个 Context
     * @param resultId resultId（唯一ID key）
     * @return
     */
    public static Context getContext(String resultId) {
        return contexts.get(resultId);
    }

    /**
     * 根据 resultId 得到一个 Context
     * @param resultId resultId（唯一ID key）
     * @return
     */
    public static Context getContext(int resultId) {
        return getContext(String.valueOf(resultId));
    }

    /**
     * 根据 resultId 删除一个 Context
     * @param resultId resultId（唯一ID key）
     * @return
     */
    public static Context removeContext(String resultId) {
        return contexts.remove(resultId);
    }

    /**
     * 根据 resultId 删除一个 Context
     * @param resultId resultId（唯一ID key）
     * @return
     */
    public static Context removeContext(int resultId) {
        return removeContext(String.valueOf(resultId));
    }
}
