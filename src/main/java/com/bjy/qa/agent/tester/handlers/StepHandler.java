package com.bjy.qa.agent.tester.handlers;

import com.alibaba.fastjson.JSONObject;
import com.bjy.qa.agent.enumtype.ConditionEnum;
import com.bjy.qa.agent.model.HandleDes;
import com.bjy.qa.agent.tester.RunStepThread;

/**
 * 步骤处理器 接口
 */
public interface StepHandler {

    /**
     * 执行步骤
     * 如果返回null则表示任务停止了
     * @param step 步骤 json
     * @param handleDes HandleDes 模型
     * @param thread RunStepThread
     * @return
     * @throws Throwable
     */
    HandleDes runStep(JSONObject step, HandleDes handleDes, RunStepThread thread) throws Throwable;

    /**
     * 获取步骤类型
     * @return
     */
    ConditionEnum getCondition();

    /**
     * 处理公共步骤
     * @param step
     * @return
     */
    default JSONObject handlerPublicStep(JSONObject step) {
        if (step.containsKey("pubSteps")) {
            return step;
        }
        return new JSONObject() {
            {
                put("step", step);
            }
        };
    }
}
