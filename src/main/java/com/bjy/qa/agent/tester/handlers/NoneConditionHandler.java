package com.bjy.qa.agent.tester.handlers;

import com.alibaba.fastjson.JSONObject;
import com.bjy.qa.agent.enumtype.ConditionEnum;
import com.bjy.qa.agent.exception.BreakException;
import com.bjy.qa.agent.model.HandleDes;
import com.bjy.qa.agent.tester.RunStepThread;
import com.bjy.qa.agent.tester.handler.debug.DebugTesterRunStepThread;
import com.bjy.qa.agent.tester.handler.debug.DebugTesterStepHandler;
import com.bjy.qa.agent.tester.handler.perf.PerfTesterRunStepThread;
import com.bjy.qa.agent.tester.handler.perf.PerfTesterStepHandler;
import com.bjy.qa.agent.tester.handler.tester.TesterRunStepThread;
import com.bjy.qa.agent.tester.handler.tester.TesterStepHandler;
import org.springframework.stereotype.Component;

/**
 * 非条件步骤
 */
@Component
public class NoneConditionHandler implements StepHandler {
    /**
     * 非条件步骤
     * @param stepJSON 步骤 json
     * @param handleDes HandleDes 模型
     * @param thread RunStepThread
     * @return
     * @throws Throwable
     */
    @Override
    public HandleDes runStep(JSONObject stepJSON, HandleDes handleDes, RunStepThread thread) throws Throwable {
        if (thread.isStopped()) {
            return null;
        }
        handleDes.clear();

        if (thread instanceof TesterRunStepThread) {
            TesterRunStepThread testerRunStepThread = (TesterRunStepThread) thread;
            TesterStepHandler testerStepHandler = testerRunStepThread.getTesterTaskBootThread().getTesterStepHandler();
            testerStepHandler.runStep(stepJSON, handleDes);
        } else if (thread instanceof DebugTesterRunStepThread) {
            DebugTesterRunStepThread debugTesterRunStepThread = (DebugTesterRunStepThread) thread;
            DebugTesterStepHandler debugTesterStepHandler = debugTesterRunStepThread.getDebugTesterTaskBootThread().getDebugTesterStepHandler();
            debugTesterStepHandler.runStep(stepJSON, handleDes);
        } else if (thread instanceof PerfTesterRunStepThread) {
            PerfTesterRunStepThread perfTesterRunStepThread = (PerfTesterRunStepThread) thread;
            PerfTesterStepHandler perfTesterStepHandler = perfTesterRunStepThread.getTesterTaskBootThread().getPerfTesterStepHandler();
            perfTesterStepHandler.runStep(stepJSON, handleDes);
        } else {
            throw new BreakException("不识别的 Condition : " + thread.getClass());
        }

        return handleDes;
    }

    /**
     * 获取步骤类型
     * @return
     */
    @Override
    public ConditionEnum getCondition() {
        return ConditionEnum.NONE;
    }
}
