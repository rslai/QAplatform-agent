package com.bjy.qa.agent.tester.handler.perf;

import com.alibaba.fastjson.JSONObject;
import com.bjy.qa.agent.enumtype.RunStatus;
import com.bjy.qa.agent.exception.ExceptionUtils;
import com.bjy.qa.agent.exception.MyException;
import com.bjy.qa.agent.model.HandleDes;
import com.bjy.qa.agent.tester.RunStepThread;
import com.bjy.qa.agent.tester.handlers.StepHandlers;
import com.bjy.qa.agent.tools.SpringTool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * PerfTester 测试任务步骤运行线程
 */
public class PerfTesterRunStepThread extends RunStepThread {
    private final Logger logger = LoggerFactory.getLogger(PerfTesterRunStepThread.class);

    public final static String STEP_TASK_PRE = "perf-tester-run-task-rid%s-cid%s"; // 占用符逻辑参考

    private final PerfTesterTaskBootThread perfTesterTaskBootThread; // 性能测试任务启动线程（Tester 启动各个子任务的线程）

    /**
     * 构造函数
     * @param perfTesterTaskBootThread
     */
    public PerfTesterRunStepThread(PerfTesterTaskBootThread perfTesterTaskBootThread) {
        this.perfTesterTaskBootThread = perfTesterTaskBootThread;

        this.setDaemon(true); // 设置为守护线程（当JVM中的所有用户线程结束时,该守护线程也会被自动终止）
        this.setName(perfTesterTaskBootThread.formatThreadName(STEP_TASK_PRE)); // 设置线程名称
        setLogTool(perfTesterTaskBootThread.getPerfTesterStepHandler().getLog()); // 设置上报运行结果工具类
    }

    /**
     * 获取性能测试任务启动线程（Tester 启动各个子任务的线程）
     * @return
     */
    public PerfTesterTaskBootThread getTesterTaskBootThread() {
        return perfTesterTaskBootThread;
    }

    /**
     * 获取测试步骤处理器
     * @return
     */
    @Override
    public void run() {
        StepHandlers stepHandlers = SpringTool.getBean(StepHandlers.class); // 获取步骤处理器
        JSONObject jsonObject = perfTesterTaskBootThread.getJsonObject(); // 获取测试任务信息

        // 之前后续代码通过 getJSONObject("step") 实现，所以这里生成一个 step 对象，向下传递
        JSONObject step = new JSONObject();
        step.put("step", jsonObject);

        // 运行步骤
        HandleDes handleDes = new HandleDes();
        if (isStopped()) {
            return;
        }
        try {
            stepHandlers.runStep(step, handleDes, this);
        } catch (MyException e) { // 业务逻辑抛出的异常会被外层 catch 后抛出，这里无须处理
        } catch (AssertionError e) { // 断言业务逻辑抛出的异常会被外层 catch 后抛出，这里无须处理
        } catch (Throwable e) {
            perfTesterTaskBootThread.getPerfTesterStepHandler().logUtil.sendStepLog(RunStatus.ERROR, handleDes.getStepDesc(), handleDes.getStepType(), ExceptionUtils.getStackTrace(e));
            perfTesterTaskBootThread.getPerfTesterStepHandler().logUtil.sendStatusLog(RunStatus.ERROR);
            e.printStackTrace();
        }
    }
}
