package com.bjy.qa.agent.tester.handler.debug;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.bjy.qa.agent.interceptor.tester.AfterInterceptor;
import com.bjy.qa.agent.interceptor.tester.BeforeInterceptor;
import com.bjy.qa.agent.interceptor.InterceptorConf;
import com.bjy.qa.agent.context.Context;
import com.bjy.qa.agent.context.HandlerContextMap;
import com.bjy.qa.agent.enumtype.ConditionEnum;
import com.bjy.qa.agent.enumtype.ErrorHandlingType;
import com.bjy.qa.agent.enumtype.RunStatus;
import com.bjy.qa.agent.enumtype.RunningModeStatus;
import com.bjy.qa.agent.exception.BreakException;
import com.bjy.qa.agent.exception.MyException;
import com.bjy.qa.agent.model.Api;
import com.bjy.qa.agent.model.HandleDes;
import com.bjy.qa.agent.model.KeyValueStore;
import com.bjy.qa.agent.model.Step;
import com.bjy.qa.agent.response.Response;
import com.bjy.qa.agent.tester.LogUtil;
import com.bjy.qa.agent.tools.SpringTool;
import com.bjy.qa.agent.tools.security.TripleDESUtil;
import com.bjy.qa.agent.transport.command.ExecuteCommand;
import com.bjy.qa.agent.transport.command.HttpExecuteCommand;
import com.bjy.qa.agent.transport.websocket.IWebSocketService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static com.bjy.qa.agent.tools.security.TripleDESUtil.decrypt;

/**
 * debug Tester 步骤执行器
 */
public class DebugTesterStepHandler {
    private static final Logger logger = LoggerFactory.getLogger(DebugTesterStepHandler.class);

    public LogUtil logUtil = new LogUtil(); // 设置上报运行结果工具类
    private Context context; // 将当前套件的 Context
    private int holdTime = 0; // 步骤间隔时间
    private RunStatus status = RunStatus.PASSED; // 测试任务运行状态

    private Response response = new Response(); // 保存返回数据(也可以理解成切面 - 某个步骤需要改变就改，不需要就不改，需要校验就校验)

    private static List<Constructor> beforeInterceptors = new LinkedList<>(); // 前置拦截器 列表
    private static List<Constructor> afterInterceptors = new LinkedList<>(); // 后置拦截器 列表

    static {
        InterceptorConf interceptorConf = SpringTool.getBean(InterceptorConf.class); // 获得 拦截器 配置

        // 遍历加载 tester 的前置拦截器
        for (String className : interceptorConf.getTesterBeforeInterceptors()) {
            try {
                if (StringUtils.isNotBlank(className)) {
                    Class clazz = Class.forName(className);
                    Constructor constructor = clazz.getConstructor(Map.class, Step.class); // 获取构造器对象
                    beforeInterceptors.add(constructor);
                    logger.info("加载 tester 的前置拦截器: {}", className);
                }
            } catch (Throwable e) {
                e.printStackTrace();
                logger.error("加载 tester 的前置拦截器错误，className：" + className + "\t" + e.getMessage() );
                throw new MyException("加载 tester 的前置拦截器错误，className：" + className + "\n" + e.getMessage());
            }
        }

        // 遍历加载 tester 的后置拦截器
        for (String className : interceptorConf.getTesterAfterInterceptors()) {
            try {
                if (StringUtils.isNotBlank(className)) {
                    Class clazz = Class.forName(className);
                    Constructor constructor = clazz.getConstructor(Map.class, Step.class, Response.class, Context.class); // 获取构造器对象
                    afterInterceptors.add(constructor);
                    logger.info("加载 tester 的后置拦截器: {}", className);
                }
            } catch (Throwable e) {
                e.printStackTrace();
                logger.error("加载 tester 的后置拦截器错误，className：" + className + "\t" + e.getMessage() );
                throw new MyException("加载 tester 的后置拦截器错误，className：" + className + "\n" + e.getMessage());
            }
        }
    }

    /**
     * 构造函数
     * @param catalogType 分类类型 - 上报日志类型
     * @param caseId 测试用例 ID
     * @param iterationCount 迭代次数
     * @param resultId 测试任务结果 ID
     * @param type 运行类型（DEBUGGING、TESTING）
     * @param sessionId 当 type=DEBUGGING 需要传入sessionId，服务器收到上报后通过这个 sessionId 发送到对应的 websocket
     */
    public DebugTesterStepHandler(int catalogType, int caseId, String iterationCount, int resultId, RunningModeStatus type, String sessionId) {
        logUtil.resultId = resultId;
        logUtil.catalogType = catalogType;
        logUtil.caseId = caseId;
        logUtil.iterationCount = iterationCount;
        logUtil.type = type;
        logUtil.sessionId = sessionId;

        // 根据 resultId 得到一个 Context，要是没有则创建一个新的 Context
        context = HandlerContextMap.getContext(resultId);
        if (context == null) {
            context = new Context();
            HandlerContextMap.addContext(resultId, context);
        }
    }

    /**
     * 关闭 driver
     */
    public void closeDriver() {
        // 遍历关闭 WebSocket
        for (Map.Entry<String, IWebSocketService> entry : this.context.getWebSocketServiceMap().entrySet()) {
            String key = entry.getKey();
            IWebSocketService iWebSocketService = entry.getValue();

            iWebSocketService.close("关闭 WebSocket: " + key); // 关闭 WebSocket
            iWebSocketService = null; // 置空
            this.context.getWebSocketServiceMap().remove(key); // 从 WebSocketServiceMap 中移除
        }
    }

    /**
     * 设置全局参数
     * @param jsonObject 全局参数
     */
    public void setGlobalParams(JSONObject jsonObject) {
        if (jsonObject == null) {
            return;
        }
        for (Map.Entry<String, Object> entry : jsonObject.entrySet()) {
            if (entry.getValue().toString().indexOf(TripleDESUtil.CIPHERTEXT_PREFIX) == 0) {
                this.context.addGlobalParas(entry.getKey(), decrypt(entry.getValue().toString())); // 用户参数需要解密
            } else {
                this.context.addGlobalParas(entry.getKey(), entry.getValue()); // 普通参数
            }
        }
    }

    public LogUtil getLog() {
        return logUtil;
    }

    /**
     * 执行步骤
     * @param stepJSON 步骤 JSON
     * @param handleDes 步骤描述
     * @throws Throwable
     */
    public void runStep(JSONObject stepJSON, HandleDes handleDes) throws Throwable {
        Step step = stepJSON.getObject("step", Step.class); // 得到步骤

        Thread.sleep(holdTime * 1000); // 步骤间隔

        before(handleDes, this.context.getGlobalParas(), step);

        handleDes.setStepDesc(step.getDesc());
        switch (step.getStepType()) {
            case INTERFACE_HTTP:
                http(handleDes, step.getApi(), step.getExtra1(), step.getExtra2(), step.getExtra3(), step.getExtra4(), step.getExtra5());
                break;
            case METHOD_STEP_HOLD:
            case METHOD_WAIT:
            case METHOD_WEB_SOCKET_WAIT:
            case PUBLIC_STEP:
            case INTERFACE_SSH:
            case METHOD_ASSERT:
            case INTERFACE_WEB_SOCKET:
            case INTERFACE_DATABASE:
            case METHOD_SAVE_PARAS:
            case METHOD_SET_PARAS:
            case RUN_SCRIPT:
            default:
                unknown(handleDes, step);
        }

        after(handleDes, this.context.getGlobalParas(), step, this.response, this.context);

        afterHandle(step, handleDes);
    }

    /**
     * 前置处理
     * @param globalParas
     * @param step
     */
    private void before(HandleDes handleDes, Map<String, Object> globalParas, Step step) {
        // 遍历执行 tester 的前置拦截器
        for (Constructor constructor : beforeInterceptors) {
            try {
                BeforeInterceptor beforeInterceptor = (BeforeInterceptor) constructor.newInstance(globalParas, step); // 利用构造器创建一个对象
                beforeInterceptor.execute();
            } catch (Exception e) {
                e.printStackTrace();
                throw new BreakException("运行 tester 的前置拦截器错误，className：" + constructor.getName() + "\t" + e.getMessage());
            }
        }
    }

    /**
     * 后置处理
     * @param handleDes
     * @param globalParas
     * @param step
     * @param response
     */
    private void after(HandleDes handleDes, Map<String, Object> globalParas, Step step, Response response, Context context) {
        // 遍历执行 tester 的后置拦截器
        for (Constructor constructor : afterInterceptors) {
            try {
                AfterInterceptor afterInterceptor = (AfterInterceptor) constructor.newInstance(globalParas, step, response, context); // 利用构造器创建一个对象
                afterInterceptor.execute();
            } catch (Exception e) {
                e.printStackTrace();
                throw new BreakException("运行 tester 的后置拦截器错误，className：" + constructor.getName() + "\t" + e.getMessage());
            }
        }
    }

    /**
     * 后置处理
     * @param step 步骤
     * @param handleDes handleDes
     * @throws Throwable
     */
    public void afterHandle(Step step, HandleDes handleDes) throws Throwable {
        ErrorHandlingType error = step.getErrorHandlingType();
        String stepDesc = handleDes.getStepDesc();
        String stepType = handleDes.getStepType();
        String detail = handleDes.getDetail();
        Throwable e = handleDes.getE();
        if (e == null) {
            logUtil.sendStepLog(RunStatus.PASSED, stepDesc, stepType, detail);
        } else {
            switch (error) {
                case IGNORE:
                    if (step.getConditionType().equals(ConditionEnum.NONE)) {
                        logUtil.sendStepLog(RunStatus.PASSED, stepDesc, stepType + " 异常！已忽略...", detail);
                    } else {
                        ConditionEnum conditionType = step.getConditionType();
                        String type = "「%s」步骤「%s」异常".formatted(conditionType.getName(), stepType);
                        logUtil.sendStepLog(RunStatus.ERROR, stepDesc, type, detail);
                        exceptionLog(e);
                    }
                    break;
                case WARNING:
                    logUtil.sendStepLog(RunStatus.WARN, stepDesc, stepType + " 异常！", detail);
                    setResultDetailStatus(RunStatus.WARN);
                    exceptionLog(e);
                    break;
                case STOP:
                    logUtil.sendStepLog(RunStatus.ERROR, stepDesc, stepType + " 异常！", detail);
                    setResultDetailStatus(RunStatus.FAILED);
                    exceptionLog(e);
                    throw e;
            }
            // 非条件步骤清除异常对象
            if (step.getConditionType().equals(ConditionEnum.NONE)) {
                handleDes.clear();
            }
        }
    }

    /**
     * 异常信息上报
     * @param e
     */
    public void exceptionLog(Throwable e) {
        logUtil.sendStepLog(RunStatus.WARN, "", "", "异常信息： " + e.fillInStackTrace().toString());
    }

    /**
     * 发送测试执行状态
     */
    public void sendStatus() {
        logUtil.sendStatusLog(status);
    }

    /**
     * 设置测试执行状态
     * @param status 测试执行状态
     */
    public void setResultDetailStatus(RunStatus status) {
        if (status.getValue() > this.status.getValue()) {
            this.status = status;
        }
    }

    /**
     * 未实现的 step
     * @param handleDes handleDes
     * @param step step 信息
     */
    public void unknown(HandleDes handleDes, Step step) {
        handleDes.setStepType("DebugTesterStepHandler 中未实现的 step");
        handleDes.setE(new MyException(step.toString()));

        logger.error("DebugTesterStepHandler 中未实现的 step: {}", step);
    }

    /**
     * http 请求
     * @param handleDes handleDes
     * @param api api
     * @param heads heads
     * @param urlParams urlParams
     * @param body body
     * @param cookies cookies
     * @param assertStr assert 断言
     */
    private void http(HandleDes handleDes, Api api, String heads, String urlParams, String body, String cookies, String assertStr) {
        /*===============================================================
          = 修改这里的代码时，别忘记修改 TesterStepHandler 中的 http 方法的代码 =
          =============================================================== */

        handleDes.setStepType("HTTP 请求");
        handleDes.setDetail("apiName:「" + api.getName() + "」 url：「" + api.getUrl() + "」" + " method：「" + api.getMethod() + "」" + " heads：「" + heads + "」" + " body：「" + body + "」" + " urlParams：「" + urlParams + "」" + " cookies：「" + cookies + "」" + " assert：「" + assertStr + "」");

        // http 请求
        try {
            String url = api.getUrl();
            String desc = api.getName();
            String method = api.getMethod();
            String mimeType = api.getExtra1(); // body数据类型

            if (!"POST".equalsIgnoreCase(method) && !"GET".equalsIgnoreCase(method) && !"DELETE".equalsIgnoreCase(method) && !"PUT".equalsIgnoreCase(method)) {
                throw new Exception("不支持的 method 类型 「" + method + "」，目前只支持 POST、GET、PUT、DELETE");
            }

            List<KeyValueStore> paramList = new ArrayList<>();

            // 组装 head 参数
            if (StringUtils.isNotEmpty(heads)) {
                paramList.add(new KeyValueStore("http-headers", heads));
            }

            // get 请求 body 为 {} 的时候，会在 header 中添加 Content_Length=2，这时有些服务器会报 411 错。所以当 get 且 body 为 {} 时，body 设置为空
            if ("GET".equalsIgnoreCase(method) && StringUtils.isNotEmpty(body) && "{}".equals(body)) {
                body = "";
            }

            // get 方法且 body 不为空添加 /ENTITY（以便发送带 body 的 get 请求）
            if ("GET".equalsIgnoreCase(method) && StringUtils.isNotEmpty(body)) {
                method = method + "/ENTITY";
            }

            // 组装 urlParams 参数
            if (StringUtils.isNotEmpty(urlParams)) {
                if ("GET".equalsIgnoreCase(method) && StringUtils.isEmpty(body)) { // 如果 get 方法 且 body 为空。不重新生成 url，直接用 paramList 向下层传 urlParams
                    JSON.parseObject(urlParams).forEach((k, v) -> {
                        if (v == null) {
                            paramList.add(new KeyValueStore(k, v));
                        } else {
                            paramList.add(new KeyValueStore(k, v.toString()));
                        }
                    });
                } else { // 否则，根据 urlParams 参数拼接带参数的 url
                    List<KeyValueStore> tmpList = new ArrayList<>();
                    JSON.parseObject(urlParams).forEach((k, v) -> {
                        if (v == null) {
                            tmpList.add(new KeyValueStore(k, v));
                        } else {
                            tmpList.add(new KeyValueStore(k, v.toString()));
                        }
                    });
                    url = this.context.getHttpService().getHttpGetURL(url, tmpList);
                }
            }

            // 组装 body 参数
            if (StringUtils.isNotEmpty(body)) {
                if ("JSON".equalsIgnoreCase(mimeType)) {
                    paramList.add(new KeyValueStore("param", body));
                } else if ("FORM".equalsIgnoreCase(mimeType)) {
                    JSON.parseObject(body).forEach((k, v) -> {
                        if (v instanceof String || v instanceof Map) {
                            paramList.add(new KeyValueStore(k, v));
                        } else {
                            throw new MyException("当 body 为 FORM 表单时，所有参数只能是 String 类型。key: " + k + "， value: " + v);
                        }
                    });
                } else {
                    throw new Exception("不支持的 body 类型 「" + mimeType + "」，目前 body 只支持 FORM、JSON");
                }
            }

            // 发送 http 请求
            ExecuteCommand executeCommand = new HttpExecuteCommand(this.context.getHttpService(), String.valueOf(logUtil.resultId), String.valueOf(logUtil.caseId), String.valueOf(logUtil.iterationCount), url, method, desc);
            this.response = executeCommand.execute(paramList);
            logUtil.sendStepLog(RunStatus.INFO, handleDes.getStepDesc(), "", response.getBody().toString());

            /* debug 不需要请求后断言 注释
            // 请求后断言
            if (!"{}".equals(assertStr)) {
                try {
                    Map<String, String> exp = new HashMap<>();
                    exp.put("body", assertStr);

                    this.response.verify(exp);
                } catch (Throwable e) {
                    handleDes.setE(e);
                }
            }*/
        } catch (Exception e) {
            handleDes.setE(e);
        }
    }
}
