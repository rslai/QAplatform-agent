package com.bjy.qa.agent.tester.handlers;

import com.alibaba.fastjson.JSONObject;
import com.bjy.qa.agent.enumtype.ConditionEnum;
import com.bjy.qa.agent.enumtype.RunStatus;
import com.bjy.qa.agent.model.HandleDes;
import com.bjy.qa.agent.tester.RunStepThread;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * if 步骤
 */
@Component
public class IfHandler implements StepHandler {
    @Autowired
    private NoneConditionHandler noneConditionHandler;
    @Autowired
    private StepHandlers stepHandlers;

    /**
     * if 步骤
     * @param stepJSON 步骤 json
     * @param handleDes HandleDes 模型
     * @param thread RunStepThread
     * @return
     * @throws Throwable
     */
    @Override
    public HandleDes runStep(JSONObject stepJSON, HandleDes handleDes, RunStepThread thread) throws Throwable {
        if (thread.isStopped()) {
            return null;
        }
        handleDes.clear();

        // 取出 if 下的步骤集合
        JSONObject conditionStep = stepJSON.getJSONObject("step");
        List<JSONObject> steps = conditionStep.getJSONArray("childSteps").toJavaList(JSONObject.class);

        String stepDesc = conditionStep.getString("desc"); // 步骤描述

        // 执行条件步骤
        thread.getLogTool().sendStepLog(RunStatus.PASSED, stepDesc, "开始执行「if」步骤", "");
        noneConditionHandler.runStep(stepJSON, handleDes, thread);
        // 上述步骤无异常则取出 if 下的步骤，再次丢给 stepHandlers 处理
        if (handleDes.getE() == null) {
            thread.getLogTool().sendStepLog(RunStatus.PASSED, stepDesc, "「if」步骤通过，开始执行子步骤", "");
            for (JSONObject step : steps) {
                stepHandlers.runStep(handlerPublicStep(step), handleDes, thread);
            }
            thread.getLogTool().sendStepLog(RunStatus.PASSED, stepDesc, "「if」子步骤执行完毕", "");
        } else {
            thread.getLogTool().sendStepLog(RunStatus.WARN, stepDesc, "「if」步骤执行失败，跳过", "");
        }
        return handleDes;
    }

    /**
     * 获取步骤类型
     * @return
     */
    @Override
    public ConditionEnum getCondition() {
        return ConditionEnum.IF;
    }
}
