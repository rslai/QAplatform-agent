package com.bjy.qa.agent.tester;

/**
 * RunStepThread
 */
public class RunStepThread extends Thread {
    protected volatile boolean stopped = false; // 是否停止
    protected LogUtil logUtil; // log工具类

    public LogUtil getLogTool() {
        return logUtil;
    }

    public void setLogTool(LogUtil logUtil) {
        this.logUtil = logUtil;
    }

    public boolean isStopped() {
        return stopped;
    }

    public void setStopped(boolean stopped) {
        this.stopped = stopped;
    }
}
