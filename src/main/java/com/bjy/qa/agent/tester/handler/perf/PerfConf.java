package com.bjy.qa.agent.tester.handler.perf;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Arrays;

/**
 * 性能测试 配置信息
 */
@Component
public class PerfConf {
    // 是否开启 debug 模式
    @Value("${qap.performance.debug:false}")
    private Boolean debug;

    // conda 的 config 信息（可以添加镜像源或其他 conda 配置）
    @Value("${qap.performance.conda-config:}")
    private String[] condaConfig;

    public Boolean getDebug() {
        return debug;
    }

    public void setDebug(Boolean debug) {
        this.debug = debug;
    }

    public String[] getCondaConfig() {
        return condaConfig;
    }

    public void setCondaConfig(String[] condaConfig) {
        this.condaConfig = condaConfig;
    }

    @Override
    public String toString() {
        return "PerfConf{" +
                "debug=" + debug +
                ", condaConfig=" + Arrays.toString(condaConfig) +
                '}';
    }
}
