package com.bjy.qa.agent.tester.handlers;

import com.alibaba.fastjson.JSONObject;
import com.bjy.qa.agent.enumtype.ConditionEnum;
import com.bjy.qa.agent.enumtype.IConditionEnum;
import com.bjy.qa.agent.exception.BreakException;
import com.bjy.qa.agent.model.HandleDes;
import com.bjy.qa.agent.tester.RunStepThread;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 步骤处理器
 */
@Component
public class StepHandlers implements ApplicationListener<ContextRefreshedEvent> {
    private final Logger logger = LoggerFactory.getLogger(StepHandlers.class);

    private final ConcurrentHashMap<ConditionEnum, StepHandler> stepHandlers = new ConcurrentHashMap<>(8);

    /**
     * 执行步骤
     * @param stepJSON 步骤JSON
     * @param handleDes 处理描述
     * @param thread 线程
     * @return
     * @throws Throwable
     */
    public HandleDes runStep(JSONObject stepJSON, HandleDes handleDes, RunStepThread thread) throws Throwable {
        JSONObject step = stepJSON.getJSONObject("step");
        Integer conditionType = step.getInteger("conditionType");
        if (conditionType == null) {
            throw new BreakException("step 中的 conditionType 不能为空！");
        }
        getSupportedCondition(IConditionEnum.valueToEnum(ConditionEnum.class, conditionType)).runStep(stepJSON, handleDes, thread);
        return handleDes;
    }

    /**
     * 添加步骤处理器
     * @param stepHandlers 步骤处理器
     * @return
     */
    @NonNull
    public StepHandlers addConditionHandlers(@Nullable Collection<StepHandler> stepHandlers) {
        if (!CollectionUtils.isEmpty(stepHandlers)) {
            for (StepHandler handler : stepHandlers) {
                if (this.stepHandlers.containsKey(handler.getCondition())) {
                    throw new RuntimeException("Same condition type implements must be unique");
                }
                this.stepHandlers.put(handler.getCondition(), handler);
            }
        }
        return this;
    }

    /**
     * 获取支持的步骤处理器
     * @param conditionEnum 条件枚举
     * @return
     */
    private StepHandler getSupportedCondition(ConditionEnum conditionEnum) {
        StepHandler handler = stepHandlers.getOrDefault(conditionEnum, null);
        if (handler == null) {
            throw new RuntimeException("condition handler for 「" + conditionEnum + "」 not found");
        }
        return handler;
    }

    /**
     * 注册所有步骤处理器（if、else、if else、none、while）
     * @param event
     */
    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        addConditionHandlers(event.getApplicationContext().getBeansOfType(StepHandler.class).values());
        logger.info("Registered {} condition handler(s)", stepHandlers.size());
    }
}
