package com.bjy.qa.agent.tester;

import com.alibaba.fastjson.JSON;
import com.bjy.qa.agent.context.HandlerContextMap;
import org.testng.ISuite;
import org.testng.ISuiteListener;

import java.util.concurrent.ConcurrentHashMap;

/**
 * testng 套件运行监听器
 */
public class SuiteListener implements ISuiteListener {
    public static ConcurrentHashMap<String, Boolean> runningTestsMap = new ConcurrentHashMap<>();

    /**
     * 在 SuiteRunner 启动之前调用此方法
     * @param suite 套件
     */
    @Override
    public void onStart(ISuite suite) {
        String rid = JSON.parseObject(suite.getParameter("casesInfo")).getString("rid");
        runningTestsMap.put(rid, true);
    }

    /**
     * 在 SuiteRunner 运行完所有测试套件后调用此方法
     * @param suite 套件
     */
    @Override
    public void onFinish(ISuite suite) {
        String rid = JSON.parseObject(suite.getParameter("casesInfo")).getString("rid");
        runningTestsMap.remove(rid);

        HandlerContextMap.removeContext(rid); // 将当前套件的 Context 清除
    }
}
