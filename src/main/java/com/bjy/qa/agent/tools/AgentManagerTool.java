package com.bjy.qa.agent.tools;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;

/**
 * Agent 管理类
 */
@Component
public class AgentManagerTool {
    private final static Logger logger = LoggerFactory.getLogger(AgentManagerTool.class);

    private static ConfigurableApplicationContext context;

    @Autowired
    public void setContext(ConfigurableApplicationContext c) {
        AgentManagerTool.context = c; // 保存当前 context
    }

    /**
     * 可以通过调用此方法结束 springboot 进程
     */
    public static void stop() {
        try {
            context.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.info("Bye！");
        System.exit(0);
    }
}
