package com.bjy.qa.agent.tools.python;

import com.bjy.qa.agent.exception.MyException;
import com.jcraft.jsch.JSchException;
import com.rslai.commons.ssh.SSH;
import com.rslai.commons.ssh.SSHResponse;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class Python3Helper extends PythonHelper {
    public Python3Helper(String[] condaConfig, PythonVersion pythonVersion, SSH ssh, String home) throws JSchException, IOException, TimeoutException {
        super(condaConfig, pythonVersion, ssh, home);
    }

    public Python3Helper(String[] condaConfig, PythonVersion pythonVersion, SSH ssh, String home, boolean debug) throws JSchException, IOException, TimeoutException {
        super(condaConfig, pythonVersion, ssh, home, debug);
    }

    @Override
    public boolean upgradePip() throws JSchException, IOException, TimeoutException {
        debugLogger("升级 pip");
        executeCommand("pip install --upgrade pip -i http://mirrors.aliyun.com/pypi/simple --trusted-host mirrors.aliyun.com");
        SSHResponse sshResponse = executeCommand("pip --version");
        if (!sshResponse.getBody().contains("pip ") || !sshResponse.getBody().contains("(python 3.")) {
            throw new MyException(String.format("升级 pip 失败。执行 %s 命令失败，错误信息：%s", sshResponse.getCmd(), sshResponse.getBody()));
        }
        debugLogger("升级 pip，成功");
        return true;
    }

    @Override
    public boolean checkLocustInstalled() throws JSchException, IOException, TimeoutException {
        debugLogger("检测是否安装 locust");
        SSHResponse sshResponse = executeCommand("pip list | grep --color=never \"locust\"");
        if (sshResponse.getBody().contains("locust") && sshResponse.getBody().contains("2.8.6")) {
            debugLogger("检测是否安装 locust，已安装");
            return true;
        } else {
            debugLogger("检测是否安装 locust，未安装");
            return false;
        }
    }

    @Override
    public boolean installLocust() throws JSchException, IOException, TimeoutException {
        debugLogger("安装 locust");
        SSHResponse sshResponse = executeCommand("pip install locust==2.8.6 -i http://mirrors.aliyun.com/pypi/simple --trusted-host mirrors.aliyun.com");
        if (!sshResponse.getBody().contains("Successfully installed")) {
            throw new MyException(String.format("安装 locust 失败。执行 %s 命令失败，错误信息：%s", sshResponse.getCmd(), sshResponse.getBody()));
        }

        sshResponse = executeCommand("pip list | grep --color=never \"locust\"");
        if (!sshResponse.getBody().contains("locust") || !sshResponse.getBody().contains("2.8.6")) {
            throw new MyException(String.format("安装 locust 失败。执行 %s 命令后未发现 locust 2.8.6，错误信息：%s", sshResponse.getCmd(), sshResponse.getBody()));
        }
        debugLogger("安装 locust，成功");
        return true;
    }
}
