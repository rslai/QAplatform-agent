package com.bjy.qa.agent.tools.http;

import okhttp3.Headers;
import okhttp3.ResponseBody;

import java.io.IOException;

public class Response {
    public static final String CONTENT_TYPE = "Content-Type";

    private int code;
    private Headers requestHeaders;
    private Headers responseHeaders;
    private Object body;

    public Response(Headers requestHeaders, int code, Headers responseHeaders, ResponseBody body) throws IOException {
        this.requestHeaders = requestHeaders;
        this.code = code;
        this.responseHeaders = responseHeaders;
        setBody(body);
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Headers getRequestHeaders() {
        return requestHeaders;
    }

    public void setRequestHeaders(Headers requestHeaders) {
        this.requestHeaders = requestHeaders;
    }

    public Headers getResponseHeaders() {
        return responseHeaders;
    }

    public void setResponseHeaders(Headers responseHeaders) {
        this.responseHeaders = responseHeaders;
    }

    public void setBody(ResponseBody body) throws IOException {
        this.body = body.string();
    }

    public <T> T getBody() throws IOException {
        return (T) body;
    }

}
