package com.bjy.qa.agent.tools.python;

import com.bjy.qa.agent.exception.MyException;
import com.jcraft.jsch.JSchException;
import com.rslai.commons.ssh.SSH;
import com.rslai.commons.ssh.SSHResponse;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class Python2Helper extends PythonHelper {
    public Python2Helper(String[] condaConfig, PythonVersion pythonVersion, SSH ssh, String home) throws JSchException, IOException, TimeoutException {
        super(condaConfig, pythonVersion, ssh, home);
    }

    public Python2Helper(String[] condaConfig, PythonVersion pythonVersion, SSH ssh, String home, boolean debug) throws JSchException, IOException, TimeoutException {
        super(condaConfig, pythonVersion, ssh, home, debug);
    }

    @Override
    public boolean upgradePip() throws JSchException, IOException, TimeoutException {
        debugLogger("升级 pip");
        executeCommand("pip install --upgrade pip -i http://mirrors.aliyun.com/pypi/simple --trusted-host mirrors.aliyun.com");
        SSHResponse sshResponse = executeCommand("pip --version");
        if (!sshResponse.getBody().contains("pip ") || !sshResponse.getBody().contains("(python 2.")) {
            throw new MyException(String.format("升级 pip 失败。执行 %s 命令失败，错误信息：%s", sshResponse.getCmd(), sshResponse.getBody()));
        }
        debugLogger("升级 pip，成功");
        return true;
    }

    @Override
    public boolean checkLocustInstalled() throws JSchException, IOException, TimeoutException {
        return false;
    }

    @Override
    public boolean installLocust() throws JSchException, IOException, TimeoutException {
        throw new MyException("pip 安装 locust 失败。python2 不再支持 locust");
    }
}
