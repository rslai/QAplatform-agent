package com.bjy.qa.agent.tools.juel;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Juel 配置信息
 */
@Component
public class JuelConf {
    // base 字符串
    @Value("${qap.juel.str_char:abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789}")
    private String strChar;

    // base 特殊字符串
    @Value("${qap.juel.special_char:!@#$%^&*()_+-<>?/'|\"\\{}[]~`}")
    private String specialChar;

    public String getStrChar() {
        return strChar;
    }

    public void setStrChar(String strChar) {
        this.strChar = strChar;
    }

    public String getSpecialChar() {
        return specialChar;
    }

    public void setSpecialChar(String specialChar) {
        this.specialChar = specialChar;
    }

    @Override
    public String toString() {
        return "JuelConf{" +
                "strChar='" + strChar + '\'' +
                ", specialChar='" + specialChar + '\'' +
                '}';
    }
}
