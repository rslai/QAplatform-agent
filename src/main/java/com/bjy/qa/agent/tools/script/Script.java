package com.bjy.qa.agent.tools.script;

import com.bjy.qa.agent.response.Response;

import java.util.Map;

public interface Script {

    /**
     * 执行脚本
     * @param response 上一步的返回结果
     * @param globalParas 全局变量
     * @param script 脚本
     * @return
     */
    Response run(Response response, Map<String, Object> globalParas, String script);

}
