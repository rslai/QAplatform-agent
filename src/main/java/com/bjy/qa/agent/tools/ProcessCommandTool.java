package com.bjy.qa.agent.tools;

import com.bjy.qa.agent.exception.BreakException;
import com.bjy.qa.agent.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.List;

public class ProcessCommandTool {
    private static final Logger logger = LoggerFactory.getLogger(ProcessCommandTool.class);

    /**
     * 执行本地命令
     * @param commandLine 命令
     * @return 返回执行结果
     */
    public static String getProcessLocalCommandStr(String commandLine) {
        List<String> processLocalCommand = getProcessLocalCommand(commandLine);
        StringBuilder stringBuilder = new StringBuilder("");
        for (String s : processLocalCommand) {
            stringBuilder.append(s);
        }
        return stringBuilder.toString();
    }

    /**
     * 执行本地命令
     * @param commandLine 命令
     * @return 返回执行结果 list，每行一个元素
     */
    public static List<String> getProcessLocalCommand(String commandLine) {
        Process process = null;
        InputStreamReader inputStreamReader = null;
        InputStreamReader errorStreamReader;
        LineNumberReader consoleInput = null;
        LineNumberReader consoleError = null;
        String consoleInputLine;
        String consoleErrorLine;
        List<String> sdrResult = new ArrayList<String>();
        List<String> sdrErrorResult = new ArrayList<String>();
        try {
            String system = System.getProperty("os.name").toLowerCase();
            if (system.contains("win")) {
                process = Runtime.getRuntime().exec(new String[]{"cmd", "/c", commandLine});
            } else {
                process = Runtime.getRuntime().exec(new String[]{"sh", "-c", commandLine});
            }
            inputStreamReader = new InputStreamReader(process.getInputStream(), System.getProperty("sun.jnu.encoding"));
            consoleInput = new LineNumberReader(inputStreamReader);
            while ((consoleInputLine = consoleInput.readLine()) != null) {
                sdrResult.add(consoleInputLine);
            }
            errorStreamReader = new InputStreamReader(process.getErrorStream(), System.getProperty("sun.jnu.encoding"));
            consoleError = new LineNumberReader(errorStreamReader);
            while ((consoleErrorLine = consoleError.readLine()) != null) {
                sdrErrorResult.add(consoleErrorLine);
            }

            int resultCode = process.waitFor();
            if (resultCode > 0 && sdrErrorResult.size() > 0) {
                return sdrErrorResult;
            } else {
                return sdrResult;
            }
        } catch (Exception e) {
            String errMsg = ExceptionUtils.getStackTrace(e);
            logger.error(errMsg);
            throw new BreakException(errMsg);
        } finally {
            try {
                if (null != consoleInput) {
                    consoleInput.close();
                }
                if (null != consoleError) {
                    consoleError.close();
                }
                if (null != inputStreamReader) {
                    inputStreamReader.close();
                }
                if (null != process) {
                    process.destroy();
                }
            } catch (Exception e) {
            }
        }
    }
}
