package com.bjy.qa.agent.tools.http;

import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Params<T> {
    private Map<String, T> params;

    /**
     * 构造函数
     */
    public Params() {
        params = new HashMap<String, T>();
    }

    /**
     * 添加一个参数
     * @param key 参数名
     * @param value 参数值
     */
    public void add(String key, T value) {
        params.put(key, value);
    }

    /**
     * 返回 entrySet
     * @return Set<Map.Entry<String, T>>
     */
    public Set<Map.Entry<String, T>> entrySet() {
        return params.entrySet();
    }

    /**
     * 返回 keySet
     * @return Set<String>
     */
    public Set<String> keySet() {
        return params.keySet();
    }

    /**
     * 返回参数个数
     * @return int
     */
    public int size() {
        return params.size();
    }

    @Override
    public String toString() {
        return "Params{" +
                "params=" + params +
                '}';
    }

    /**
     * 返回 Json 形式的参数字符串
     * @return String
     */
    public String toJsonString() {
        return new Gson().toJson(params);
    }

}
