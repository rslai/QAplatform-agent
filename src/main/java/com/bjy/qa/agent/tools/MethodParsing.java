package com.bjy.qa.agent.tools;

import com.bjy.qa.agent.exception.MyException;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * 方法解析
 */
public class MethodParsing {
    private Pattern methodPattern = Pattern.compile("([a-zA-Z0-9]+)\\((.*?)\\)"); // 提取 方法、参数 正则

    private String str; // 方法字符串
    private String methodName; // 方法名
    private List<String> paramList = new ArrayList<>(); // 参数列表

    /**
     * 构造函数
     * @param str 待解析的方法字符串
     */
    public MethodParsing(String str) {
        this.str = str;

        // 解析方法名
        Matcher matcher = methodPattern.matcher(str);
        List<MatchResult> matchResultList = matcher.results().collect(Collectors.toList());
        if (matchResultList.size() == 1) {
            MatchResult matchResult = matchResultList.get(0);
            if (matchResult.groupCount() == 2) {
                this.methodName = matchResult.group(1); // 方法名
                paramParsing(matchResult.group(2));
            }
        }
    }

    /**
     * 解析参数
     * @param paramStr 参数字符串
     */
    private void paramParsing(String paramStr) {
        String[] splitParams = paramStr.split(",");
        for (String param : splitParams) {
            paramList.add(removeQuotes(param.trim()));
        }
    }

    /**
     * 如果是字符串参数，去除首尾引号
     * @param str 参数字符串
     * @return
     */
    private String removeQuotes(String str) {
        boolean hasQuotes = str.matches("^\".*\"$|^'.*'$"); // 判断是否有引号
        if(hasQuotes) {
            str = str.replaceAll("^\"|\"$|^'|'$", ""); // 去除首尾引号
        }
        return str;
    }

    /**
     * 获取方法名
     * @return
     */
    public String getMethodName() {
        if (methodName == null) {
            throw new MyException("语法解析错误: " + str);
        } else {
            return methodName;
        }
    }

    /**
     * 获取参数列表
     * @return
     */
    public List<String> getParamList() {
        if (methodName == null) {
            throw new MyException("语法解析错误: " + str);
        }
        return paramList;
    }
}