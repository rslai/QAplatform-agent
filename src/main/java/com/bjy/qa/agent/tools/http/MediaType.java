package com.bjy.qa.agent.tools.http;

/**
 * MediaType，FORM（form表单）, JSON（json数据）
 */
public enum MediaType {
    JSON("JSON", okhttp3.MediaType.Companion.parse("application/json; charset=" + HttpHelper.getCharset()));

    private String name;
    private okhttp3.MediaType mediaType;

    private MediaType(String name, okhttp3.MediaType mediaType) {
        this.name = name;
        this.mediaType = mediaType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public okhttp3.MediaType getMediaType() {
        return mediaType;
    }

    public void setMediaType(okhttp3.MediaType mediaType) {
        this.mediaType = mediaType;
    }

    @Override
    public String toString() {
        return name;
    }
}
