package com.bjy.qa.agent.tools.mqtt;

/**
 * QOS服务质量（默认LEVEL1）
 *
 * LEVEL0：至多一次，Sender 发送的一条消息，Receiver 最多能收到一次，也就是说 Sender 尽力向 Receiver 发送消息，如果发送失败，也就算了；
 * LEVEL1：至少一次，Sender 发送的一条消息，Receiver 至少能收到一次，也就是说 Sender 向 Receiver 发送消息，如果发送失败，会继续重试，直到 Receiver 收到消息为止，但是因为重传的原因，Receiver 有可能会收到重复的消息
 * LEVEL2：至少一次，Sender 发送的一条消息，Receiver 确保能收到而且只收到一次，也就是说 Sender 尽力向 Receiver 发送消息，如果发送失败，会继续重试，直到 Receiver 收到消息为止，同时保证 Receiver 不会因为消息重传而收到重复的消息
 */
public enum MqttQOS {
    LEVEL_0(0), LEVEL_1(1), LEVEL_2(2);

    private int value;

    private MqttQOS(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
