package com.bjy.qa.agent.tools;

import com.bjy.qa.agent.transport.http.HttpClient;
import com.bjy.qa.agent.transport.mqtt.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

/**
 * 启动工具类
 * 实现 ApplicationRunner 类，应用容器启动之后会回调到 run 方法
 */
@Component
public class LaunchTool implements ApplicationRunner {
    private static final Logger logger = LoggerFactory.getLogger(LaunchTool.class);

    @Override
    public void run(ApplicationArguments args) {
        new Thread(() -> {
            try {
                // 创建 http 连接
                boolean httpSuccess = HttpClient.init();

                // 创建 mqtt 连接
                if (httpSuccess) {
                    new MqttClient().connectMqtt(HttpClient.mqttUrl, HttpClient.mqttQos, HttpClient.mqttBaseTopic, HttpClient.agentKey, HttpClient.mqttAllowAnonymous, HttpClient.mqttUserName, HttpClient.mqtttPassword);
                }
            } catch (MqttException e) {
                e.printStackTrace();
            }
        }).start();
    }

}
