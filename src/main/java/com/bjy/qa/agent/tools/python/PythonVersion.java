package com.bjy.qa.agent.tools.python;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Pyton 版本 枚举类
 */
public enum PythonVersion {
    PY_2_7_18("2.7.18", 2718),
    PY_3_6_8("3.6.8", 368),
    PY_3_7_16("3.7.16", 3716);

    private String name;
    @JsonValue
    private int value;

    PythonVersion(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    @JSONField
    public int getValue() {
        return value;
    }
}
