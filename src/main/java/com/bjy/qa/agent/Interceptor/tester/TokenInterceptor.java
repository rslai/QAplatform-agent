package com.bjy.qa.agent.interceptor.tester;

import com.bjy.qa.agent.context.Context;
import com.bjy.qa.agent.model.Step;
import com.bjy.qa.agent.response.Response;
import com.bjy.qa.agent.tools.json.JsonHelper;

import java.util.Map;

/**
 * token Interceptor（登录成功后的后置拦截器）
 */
public class TokenInterceptor extends AfterInterceptor {
    /**
     * 后置 拦截器 构造方法
     * @param globalParas 全局参数
     * @param step step 数据
     * @param response 返回数据(也可以理解成切面 - 某个步骤需要改变就改，不需要就不改，需要校验就校验)
     * @param context 当前套件的 Context
     */
    public TokenInterceptor(Map<String, Object> globalParas, Step step, Response response, Context context) {
        super(globalParas, step, response, context);
    }

    @Override
    public void execute(String url, Response response, Context context) {
        if (url.indexOf("/user/user/login") >= 0) { // 统一测试平台 login 后置拦截器
            this.qapLogin(response, context);
        }
    }

    /**
     * 统一测试平台 login 后置拦截器
     * @param response 返回数据(也可以理解成切面 - 某个步骤需要改变就改，不需要就不改，需要校验就校验)
     * @param context 当前套件的 Context
     */
    private void qapLogin(Response response, Context context) {
        JsonHelper jsonHelper = new JsonHelper(response.getBody()); // 将请求返回的内容转成 JsonHelper

        Integer code = jsonHelper.getInteger("$.code");
        if (code == 0) {
            String token = jsonHelper.getString("$.data.token");
            context.getHttpService().addHeader("token", token);
        }
    }
}
