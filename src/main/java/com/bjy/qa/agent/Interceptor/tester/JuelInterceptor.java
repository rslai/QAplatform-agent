package com.bjy.qa.agent.interceptor.tester;

import com.bjy.qa.agent.exception.MyException;
import com.bjy.qa.agent.model.Step;
import com.bjy.qa.agent.tools.juel.JuelHelper;
import org.apache.commons.lang.StringUtils;

import java.util.Map;

/**
 * Juel Interceptor（使用 Juel 表达式实现参数替换，例如：将 ${aa} 替换成 aa 的值）
 */
public class JuelInterceptor extends BeforeInterceptor {
    /**
     * 前置 拦截器 构造防范
     * @param globalParas 全局参数
     * @param step step 数据
     */
    public JuelInterceptor(Map<String, Object> globalParas, Step step) {
        super(globalParas, step);
    }

    @Override
    protected String executeUrl(Map<String, Object> globalParas, String url) {
        try {
            if (StringUtils.isBlank(url)) {
                return url;
            } else {
                return JuelHelper.getValue(globalParas, url);
            }
        } catch (Exception e) {
            throw new MyException("Juel 表达式异常：" + e.getMessage());
        }
    }

    @Override
    protected String executeExtra1(String url, Map<String, Object> globalParas, String extra) {
        try {
            if (StringUtils.isBlank(extra)) {
                return extra;
            } else {
                return JuelHelper.getValue(globalParas, extra);
            }
        } catch (Exception e) {
            throw new MyException("Juel 表达式异常：" + e.getMessage());
        }
    }

    @Override
    protected String executeExtra2(String url, Map<String, Object> globalParas, String extra) {
        try {
            if (StringUtils.isBlank(extra)) {
                return extra;
            } else {
                return JuelHelper.getValue(globalParas, extra);
            }
        } catch (Exception e) {
            throw new MyException("Juel 表达式异常：" + e.getMessage());
        }
    }

    @Override
    protected String executeExtra3(String url, Map<String, Object> globalParas, String extra) {
        try {
            if (StringUtils.isBlank(extra)) {
                return extra;
            } else {
                return JuelHelper.getValue(globalParas, extra);
            }
        } catch (Exception e) {
            throw new MyException("Juel 表达式异常：" + e.getMessage());
        }
    }

    @Override
    protected String executeExtra4(String url, Map<String, Object> globalParas, String extra) {
        try {
            if (StringUtils.isBlank(extra)) {
                return extra;
            } else {
                return JuelHelper.getValue(globalParas, extra);
            }
        } catch (Exception e) {
            throw new MyException("Juel 表达式异常：" + e.getMessage());
        }
    }

    @Override
    protected String executeExtra5(String url, Map<String, Object> globalParas, String extra) {
        try {
            if (StringUtils.isBlank(extra)) {
                return extra;
            } else {
                return JuelHelper.getValue(globalParas, extra);
            }
        } catch (Exception e) {
            throw new MyException("Juel 表达式异常：" + e.getMessage());
        }
    }
}
