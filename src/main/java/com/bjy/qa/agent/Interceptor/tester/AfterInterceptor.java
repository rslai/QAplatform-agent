package com.bjy.qa.agent.interceptor.tester;

import com.bjy.qa.agent.context.Context;
import com.bjy.qa.agent.model.Step;
import com.bjy.qa.agent.response.Response;

import java.util.Map;

import static com.bjy.qa.agent.enumtype.CatalogType.*;

/**
 * 后置 拦截器
 */
public abstract class AfterInterceptor {
    private Map<String, Object> globalParas; // 全局参数
    private Step step; // step 数据
    private Response response; // 返回数据(也可以理解成切面 - 某个步骤需要改变就改，不需要就不改，需要校验就校验)
    private Context context; // 当前套件的 Context

    /**
     * 后置 拦截器 构造方法
     * @param globalParas 全局参数
     * @param step step 数据
     * @param response 返回数据(也可以理解成切面 - 某个步骤需要改变就改，不需要就不改，需要校验就校验)
     * @param context 当前套件的 Context
     */
    public AfterInterceptor(Map<String, Object> globalParas, Step step, Response response, Context context) {
        this.globalParas = globalParas;
        this.step = step;
        this.response = response;
        this.context = context;
    }

    public void execute() {
        String url = "";
        if ((step.getStepType() == INTERFACE_HTTP) || (step.getStepType() == INTERFACE_WEB_SOCKET)) {
            url = step.getApi().getUrl();

            this.execute(url, response, context);
        }
    }

    /**
     * 后置拦截器 具体的处理方法
     * @param url url
     * @param response 返回数据(也可以理解成切面 - 某个步骤需要改变就改，不需要就不改，需要校验就校验)
     * @param context 当前套件的 Context
     */
    public abstract void execute(String url, Response response, Context context);
}
