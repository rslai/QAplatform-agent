package com.bjy.qa.agent.interceptor;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 拦截器 配置
 */
@Component
public class InterceptorConf {
    @Value("${qap.tester.before-interceptor:}")
    private String[] testerBeforeInterceptors; // tester 的前置拦截器（接口）

    @Value("${qap.tester.after-interceptor:}")
    private String[] testerAfterInterceptors; // tester 的后置拦截器（接口）

    @Value("${qap.performance.before-interceptor:}")
    private String[] perfBeforeInterceptors; // tester 的前置拦截器（性能）

    public String[] getTesterBeforeInterceptors() {
        return testerBeforeInterceptors;
    }

    public String[] getTesterAfterInterceptors() {
        return testerAfterInterceptors;
    }

    public String[] getPerfBeforeInterceptors() {
        return perfBeforeInterceptors;
    }
}
