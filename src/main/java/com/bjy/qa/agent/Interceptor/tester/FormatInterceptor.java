package com.bjy.qa.agent.interceptor.tester;

import com.alibaba.fastjson.JSONObject;
import com.bjy.qa.agent.model.Step;
import com.bjy.qa.agent.tools.StringUtil;

import java.util.Map;

import static com.bjy.qa.agent.enumtype.CatalogType.*;

/**
 * 格式化 Interceptor（目前会格式化 json 数据）
 */
public class FormatInterceptor extends BeforeInterceptor {
    /**
     * 前置 拦截器 构造方法
     * @param globalParas 全局参数
     * @param step step 数据
     */
    public FormatInterceptor(Map<String, Object> globalParas, Step step) {
        super(globalParas, step);
    }

    @Override
    protected String executeUrl(Map<String, Object> globalParas, String url) {
        return url;
    }

    @Override
    protected String executeExtra1(String url, Map<String, Object> globalParas, String extra) {
        if (step.getStepType() == INTERFACE_HTTP || step.getStepType() == INTERFACE_WEB_SOCKET || step.getStepType() == METHOD_SET_PARAS || step.getStepType() == METHOD_SAVE_PARAS) {
            try {
                return JSONObject.parseObject(extra).toJSONString();
            } catch (Exception e) {
                return extra;
            }
        } else if (step.getStepType() == METHOD_ASSERT) {
            try {
                return StringUtil.compressJson(extra);
            } catch (Exception e) {
                return extra;
            }
        } else {
            return extra;
        }
    }

    @Override
    protected String executeExtra2(String url, Map<String, Object> globalParas, String extra) {
        if (step.getStepType() == INTERFACE_HTTP || step.getStepType() == INTERFACE_WEB_SOCKET) {
            try {
                return JSONObject.parseObject(extra).toJSONString();
            } catch (Exception e) {
                return extra;
            }
        } else {
            return extra;
        }
    }

    @Override
    protected String executeExtra3(String url, Map<String, Object> globalParas, String extra) {
        if (step.getStepType() == INTERFACE_HTTP || step.getStepType() == INTERFACE_WEB_SOCKET) {
            try {
                return JSONObject.parseObject(extra).toJSONString();
            } catch (Exception e) {
                return extra;
            }
        } else {
            return extra;
        }
    }

    @Override
    protected String executeExtra4(String url, Map<String, Object> globalParas, String extra) {
        if (step.getStepType() == INTERFACE_HTTP || step.getStepType() == INTERFACE_WEB_SOCKET) {
            try {
                return JSONObject.parseObject(extra).toJSONString();
            } catch (Exception e) {
                return extra;
            }
        } else {
            return extra;
        }
    }

    @Override
    protected String executeExtra5(String url, Map<String, Object> globalParas, String extra) {
        if (step.getStepType() == INTERFACE_HTTP || step.getStepType() == INTERFACE_WEB_SOCKET) {
            try {
                return StringUtil.compressJson(extra);
            } catch (Exception e) {
                return extra;
            }
        } else {
            return extra;
        }
    }
}
