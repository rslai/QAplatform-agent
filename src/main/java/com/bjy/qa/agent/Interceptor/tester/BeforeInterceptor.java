package com.bjy.qa.agent.interceptor.tester;

import com.bjy.qa.agent.model.Step;

import java.util.Map;

import static com.bjy.qa.agent.enumtype.CatalogType.*;

/**
 * 前置 拦截器
 */
public abstract class BeforeInterceptor {
    private Map<String, Object> globalParas; // 全局参数
    protected Step step; // step 数据

    /**
     * 前置 拦截器 构造防范
     * @param globalParas 全局参数
     * @param step step 数据
     */
    public BeforeInterceptor(Map<String, Object> globalParas, Step step) {
        this.globalParas = globalParas;
        this.step = step;
    }

    public void execute() {
        if ((step.getStepType() == INTERFACE_HTTP) || (step.getStepType() == INTERFACE_WEB_SOCKET) || (step.getStepType() == INTERFACE_DATA_CHANNEL) || (step.getStepType() == INTERFACE_DATABASE) || (step.getStepType() == INTERFACE_SSH) || (step.getStepType() == METHOD_ASSERT)) {
            String url = null;
            if (!(step.getStepType() == METHOD_ASSERT)) { // METHOD_ASSERT 没有 url 参数所以忽略 url 处理
                url = this.executeUrl(globalParas, step.getApi().getUrl());
                step.getApi().setUrl(url);
            }

            step.setExtra1(this.executeExtra1(url, globalParas, step.getExtra1()));
            step.setExtra2(this.executeExtra2(url, globalParas, step.getExtra2()));
            step.setExtra3(this.executeExtra3(url, globalParas, step.getExtra3()));
            step.setExtra4(this.executeExtra4(url, globalParas, step.getExtra4()));
            step.setExtra5(this.executeExtra5(url, globalParas, step.getExtra5()));
        }
    }


    /**
     * 处理字段 url
     * @param globalParas 全局参数
     * @param url url
     * @return
     */
    protected abstract  String executeUrl(Map<String, Object> globalParas, String url);

    /**
     * 处理字段 extra1
     * 注意:如果无需处理直接 return extra;
     * @param url url
     * @param globalParas 全局参数
     * @param extra 参数
     * @return
     */
    protected abstract String executeExtra1(String url, Map<String, Object> globalParas, String extra);

    /**
     * 处理字段 extra2
     * 注意:如果无需处理直接 return extra;
     * @param url url
     * @param globalParas 全局参数
     * @param extra 参数
     * @return
     */
    protected abstract String executeExtra2(String url, Map<String, Object> globalParas, String extra);

    /**
     * 处理字段 extra3
     * 注意:如果无需处理直接 return extra;
     * @param url url
     * @param globalParas 全局参数
     * @param extra 参数
     * @return
     */
    protected abstract String executeExtra3(String url, Map<String, Object> globalParas, String extra);

    /**
     * 处理字段 extra4
     * 注意:如果无需处理直接 return extra;
     * @param url url
     * @param globalParas 全局参数
     * @param extra 参数
     * @return
     */
    protected abstract String executeExtra4(String url, Map<String, Object> globalParas, String extra);

    /**
     * 处理字段 extra5
     * 注意:如果无需处理直接 return extra;
     * @param url url
     * @param globalParas 全局参数
     * @param extra 参数
     * @return
     */
    protected abstract String executeExtra5(String url, Map<String, Object> globalParas, String extra);
}
