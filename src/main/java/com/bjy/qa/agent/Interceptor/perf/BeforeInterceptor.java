package com.bjy.qa.agent.interceptor.perf;

import com.bjy.qa.agent.model.PerfStep;

import java.util.Map;

/**
 * 前置（性能） 拦截器
 */
public abstract class BeforeInterceptor {
    private Map<String, Object> globalParas; // 全局参数
    protected PerfStep step; // step 数据

    /**
     * 前置（性能） 拦截器 构造防范
     * @param globalParas 全局参数
     * @param step step 数据
     */
    public BeforeInterceptor(Map<String, Object> globalParas, PerfStep step) {
        this.globalParas = globalParas;
        this.step = step;
    }

    public void execute() {
        step.setContent(this.executeContent(globalParas, step.getContent()));
    }

    /**
     * 处理字段 content（脚本）
     * 注意:如果无需处理直接 return content;
     * @param globalParas 全局参数
     * @param content 参数
     * @return
     */
    protected abstract String executeContent(Map<String, Object> globalParas, String content);
}
