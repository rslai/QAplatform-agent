package com.bjy.qa.agent.interceptor.perf;

import com.bjy.qa.agent.exception.MyException;
import com.bjy.qa.agent.model.PerfStep;
import com.bjy.qa.agent.tools.juel.JuelHelper;
import org.apache.commons.lang.StringUtils;

import java.util.Map;

/**
 * Juel（性能） Interceptor（使用 Juel 表达式实现参数替换，例如：将 ${aa} 替换成 aa 的值）
 */
public class JuelInterceptor extends BeforeInterceptor {
    /**
     * 前置（性能） 拦截器 构造防范
     * @param globalParas 全局参数
     * @param step step 数据
     */
    public JuelInterceptor(Map<String, Object> globalParas, PerfStep step) {
        super(globalParas, step);
    }

    @Override
    protected String executeContent(Map<String, Object> globalParas, String content) {
        try {
            if (StringUtils.isBlank(content)) {
                return content;
            } else {
                return JuelHelper.getValue(globalParas, content);
            }
        } catch (Exception e) {
            throw new MyException("Juel 表达式异常：" + e.getMessage());
        }
    }
}
