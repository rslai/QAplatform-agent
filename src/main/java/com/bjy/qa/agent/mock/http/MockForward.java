package com.bjy.qa.agent.mock.http;

/**
 * mock 转发
 */
public class MockForward {
    private boolean enable; // 是否开启转发，默认不转发
    private String host; // URL 请求中的 host，host + uri 是完整的转发地址
    private String uri; // 请求的 URI，host + uri 是完整的转发地址

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Forward{");
        sb.append("enable=").append(enable);
        sb.append(", host='").append(host).append('\'');
        sb.append(", uri='").append(uri).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
