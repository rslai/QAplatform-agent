package com.bjy.qa.agent.mock.http;

import com.bjy.qa.agent.tools.EnumUtil;

import java.util.HashMap;
import java.util.Map;

public class MockResponse {
    private int status; // 返回的状态码
    private MockContentType contentType; // 请求的 Content-Type
    private Map<String, String> headers = new HashMap<>(); // 返回的 headers
    private Map<String, String> cookies = new HashMap<>(); // 返回的 cookies
    private String body = ""; // 返回的 body 参数
    private MockForward mockForward; // 转发

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public MockContentType getContentType() {
        return contentType;
    }

    public void setContentType(int contentType) {
        this.contentType = EnumUtil.valueOf(MockContentType.class, contentType);
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }

    public Map<String, String> getCookies() {
        return cookies;
    }

    public void setCookies(Map<String, String> cookies) {
        this.cookies = cookies;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public MockForward getMockForward() {
        return mockForward;
    }

    public void setMockForward(MockForward mockForward) {
        this.mockForward = mockForward;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("MockResponse{");
        sb.append("status=").append(status);
        sb.append(", contentType=").append(contentType);
        sb.append(", headers=").append(headers);
        sb.append(", cookies=").append(cookies);
        sb.append(", body='").append(body).append('\'');
        sb.append(", mockForward=").append(mockForward);
        sb.append('}');
        return sb.toString();
    }
}
