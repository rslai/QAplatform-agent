package com.bjy.qa.agent.mock.http;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Mock ContentType 枚举类
 */
public enum MockContentType {
    UNKNOWN("UNKNOWN", 0),
    NONE("none", 1),
    FORM_DATA("multipart/form-data", 2),
    X_WWW_FORM_URLENCODED("application/x-www-form-urlencoded", 3),
    TEXT("text/plain", 4),
    JAVASCRIPT("application/javascript", 5),
    JSON("application/json", 6),
    HTML("text/html", 7),
    XML("application/xml", 8);
//    BINARY("application/octet-stream", 9),
//    BINARY_JPEG("image/jpeg", 10),
//    BINARY_PNG("image/png", 11),
//    BINARY_GIF("image/gif", 12),
//    BINARY_SVG("image/svg+xml", 13),
//    BINARY_ICON("image/x-icon", 14),
//    BINARY_BMP("image/bmp", 15),
//    BINARY_WEBP("image/webp", 16),
//    BINARY_TIFF("image/tiff", 17),
//    BINARY_PSD("image/vnd.adobe.photoshop", 18),
//    BINARY_MP4("video/mp4", 19),
//    BINARY_MPEG("video/mpeg", 20),
//    BINARY_WEBM("video/webm", 21),
//    BINARY_QUICKTIME("video/quicktime", 22),
//    BINARY_3GPP("video/3gpp", 23),
//    BINARY_PDF("application/pdf", 24);

    private String name;

    @JsonValue
    private int value;

    MockContentType(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    @JSONField
    public int getValue() {
        return value;
    }
}
