package com.bjy.qa.agent.model;

/**
 * HandleDes 模型
 */
public class HandleDes {
    private String stepDesc = ""; // 步骤描述
    private String stepType = ""; // 步骤类型(等待、休眠)
    private String detail = ""; // 步骤详情
    private Throwable e = null; // 步骤异常

    public void clear() {
        this.stepDesc = "";
        this.stepType = "";
        this.detail = "";
        this.e = null;
    }

    public String getStepDesc() {
        return stepDesc;
    }

    public void setStepDesc(String stepDesc) {
        this.stepDesc = stepDesc;
    }

    public String getStepType() {
        return stepType;
    }

    public void setStepType(String stepType) {
        this.stepType = stepType;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public Throwable getE() {
        return e;
    }

    public void setE(Throwable e) {
        this.e = e;
    }

    @Override
    public String toString() {
        return "HandleDes{" +
                "stepDesc='" + stepDesc + '\'' +
                ", stepType='" + stepType + '\'' +
                ", detail='" + detail + '\'' +
                ", e=" + e +
                '}';
    }
}
