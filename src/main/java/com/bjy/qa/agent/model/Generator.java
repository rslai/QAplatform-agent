package com.bjy.qa.agent.model;

import com.bjy.qa.agent.enumtype.SystemType;
import com.bjy.qa.agent.tools.EnumUtil;
import lombok.Data;

/**
 * 压力机（load generators）Entity 模型
 */
@Data
public class Generator {
    Long id; // 自增ID
    Long projectId; // 所属项目id
    private SystemType systemType; // 运行系统
    private String hostname; // 主机名
    private String ip; // IP 地址
    private int port; // 端口号
    private String workDir; // 工作目录
    private String runCmd; // 预执行命令，登录成功后需要先执行的命令
    String extra; // 扩展信息（用户名、密码等）
    String desc; // 描述

    public void setSystemType(int systemType) {
        this.systemType = EnumUtil.valueOf(SystemType.class, systemType);
    }
}
