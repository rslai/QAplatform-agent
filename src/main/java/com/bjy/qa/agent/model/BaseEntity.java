package com.bjy.qa.agent.model;

import lombok.Data;

import java.util.Date;

/**
 * Base Entity 模型
 */
@Data
public abstract class BaseEntity {
    protected Long id; // 自增ID
    private boolean isDelete; // 是否已删除
    protected Date updatedAt; // 更新时间
    protected Date createdAt; // 创建时间
}
