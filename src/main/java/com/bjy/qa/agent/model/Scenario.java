package com.bjy.qa.agent.model;

import com.bjy.qa.agent.enumtype.ControlMode;
import com.bjy.qa.agent.enumtype.LoadTestMode;
import com.bjy.qa.agent.enumtype.LogMode;
import com.bjy.qa.agent.tools.EnumUtil;
import lombok.Data;

/**
 * 性能测试场景 Entity 模型
 */
@Data
public class Scenario {
    Long id; // 自增ID
    Long projectId; // 所属项目 ID
    LogMode logMode; // 日志模式
    ControlMode controlMode; // 控制模式
    LoadTestMode loadTestMode; // 压测模式
    String content; // 测试场景内容

    public void setLogMode(int mode) {
        this.logMode = EnumUtil.valueOf(LogMode.class, mode);
    }

    public void setControlMode(int mode) {
        this.controlMode = EnumUtil.valueOf(ControlMode.class, mode);
    }

    public void setLoadTestMode(int mode) {
        this.loadTestMode = EnumUtil.valueOf(LoadTestMode.class, mode);
    }
}
