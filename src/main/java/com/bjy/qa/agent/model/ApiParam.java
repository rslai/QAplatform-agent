package com.bjy.qa.agent.model;

import lombok.Data;

/**
 * api 定义中的参数 Entity 模型
 */
@Data
public class ApiParam extends BaseEntity {
    Long apiId; // api 定义 ID
    String name; // 参数名称
    boolean isRequired; // 是否必须
    String des; // 参数说明
    String value; // 值
    Integer sort; // 排序
}
