package com.bjy.qa.agent.model;

/**
 * ExecuteCommand 中使用到的 KeyValueStore 模型
 */
public class KeyValueStore {
    String name; // key
    Object value; // value

    /**
     * 构造方法
     * @param name  name
     * @param value value
     */
    public KeyValueStore(String name, Object value) {
        this.name = name;
        this.value = value;
    }

    /**
     * 返回 name
     * @return name
     */
    public String getName() {
        return this.name;
    }

    /**
     * 返回 value
     * @return value
     */
    public Object getValue() {
        return this.value;
    }

    /**
     * @param value 设置 value
     */
    public void setValue(Object value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "KeyValueStore [" +
                "name='" + name + '\'' +
                ", value=" + value +
                ']';
    }
}
