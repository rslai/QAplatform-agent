package com.bjy.qa.agent.model;

import com.bjy.qa.agent.enumtype.CatalogType;
import com.bjy.qa.agent.tools.EnumUtil;
import lombok.Data;

/**
 * api 定义的 Entity 模型
 */
@Data
public class Api extends BaseEntity {
    Long projectId; // 所属项目id
    Long catalogId; // 所属分类 ID
    CatalogType type; // 分类类型
    String name; // api 定义名称
    String url; // api 定义 URL
    String method; // api 方法，http请求时为 POST/GET/PUT 等
    String extra1; // 扩展信息，http请求时为 JSON/FORM 等
    Integer sort = 9999; // 排序

    public void setType(int type) {
        this.type = EnumUtil.valueOf(CatalogType.class, type);
    }
}
