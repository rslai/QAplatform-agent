package com.bjy.qa.agent.model;

import com.bjy.qa.agent.enumtype.CatalogType;
import com.bjy.qa.agent.enumtype.ConditionEnum;
import com.bjy.qa.agent.enumtype.ScriptType;
import com.bjy.qa.agent.tools.EnumUtil;
import lombok.Data;

import java.util.List;

/**
 * 性能测试用例步骤 Entity 模型
 */
@Data
public class PerfStep {
    CatalogType ctype; // catalogType 分类类型 - 上报日志类型
    Long pid; // 所属项目id
    Long rid; // 运行结果 id
    Long cid; // 测试用例 ID
    ScriptType scriptType; // 性能测试脚本类型
    String name; // 用例名称
    String dataProvider; // DataProivder（数据驱动）
    String content; // 脚本内容
    ConditionEnum conditionType; // 条件类型
    List<Generator> generators; // 使用的压力机
    Scenario scenario; // 性能测试场景

    public void setCtype(int type) {
        this.ctype = EnumUtil.valueOf(CatalogType.class, type);
    }

    public void setScriptType(int type) {
        this.scriptType = EnumUtil.valueOf(ScriptType.class, type);
    }

    public void setConditionType(int conditionType) {
        this.conditionType = EnumUtil.valueOf(ConditionEnum.class, Integer.valueOf(conditionType));
    }
}
