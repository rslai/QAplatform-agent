package com.bjy.qa.agent.model;

import com.bjy.qa.agent.enumtype.CatalogType;
import com.bjy.qa.agent.enumtype.ConditionEnum;
import com.bjy.qa.agent.enumtype.ErrorHandlingType;
import com.bjy.qa.agent.tools.EnumUtil;
import lombok.Data;

/**
 * 测试用例步骤 Entity 模型
 */
@Data
public class Step {
    Long projectId; // 所属项目id
    Long testCaseId; // 测试用例 ID
    String desc; // 步骤描述
    Long apiId; // API ID
    Api api; // 一对一的 Api
    CatalogType stepType; // 步骤类型
    ErrorHandlingType errorHandlingType; // 异常处理类型
    ConditionEnum conditionType; // 条件类型
    Integer sort = 9999; // 排序
    String extra1; // 扩展信息（手工测试：操作步骤。自动化测试：head）
    String extra2; // 扩展信息（手工测试：预期结果。自动化测试：body）
    String extra3; // 扩展信息（自动化测试：http url 参数）
    String extra4; // 扩展信息（自动化测试：cookie。WebSocket 中保存通道）
    String extra5; // 扩展信息（自动化测试：assert）

    public void setStepType(int stepType) {
        this.stepType = EnumUtil.valueOf(CatalogType.class, stepType);
    }

    public void setErrorHandlingType(int errorHandlingType) {
        this.errorHandlingType = EnumUtil.valueOf(ErrorHandlingType.class, errorHandlingType);
    }

    public void setConditionType(int conditionType) {
        this.conditionType = EnumUtil.valueOf(ConditionEnum.class, Integer.valueOf(conditionType));
    }
}
