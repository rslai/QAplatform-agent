package com.bjy.qa.agent.exception;

/**
 * 不支持的返回结果类型 异常类
 */
public class UnsupportedFilterResultType extends Exception {

    public UnsupportedFilterResultType(String message) {
        super(message);
    }

}
