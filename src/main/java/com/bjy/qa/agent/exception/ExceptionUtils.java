package com.bjy.qa.agent.exception;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * 异常工具类
 */
public class ExceptionUtils {
    /**
     * 获取完整堆栈信息
     * @param throwable throwable
     * @return
     */
    public static String getStackTrace(Throwable throwable) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        try {
            throwable.printStackTrace(pw);
            return sw.toString();
        } finally {
            try {
                sw.close();
            } catch (IOException e) {
            }
            pw.close();
        }
    }
}
