package com.bjy.qa.agent.exception;

/**
 * 中断运行异常类型，遇到需要结束类型的异常用这个抛出
 */
public class BreakException extends RuntimeException {
    public BreakException(String message) {
        super(message);
    }
}
