package com.bjy.qa.agent.exception;

/**
 * 业务异常包装类
 */
public class MyException extends RuntimeException {
    public MyException(String message) {
        super(message);
    }
}
