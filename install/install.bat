echo off
CHCP 65001 > null
del /q null

goto start
::
:: windows 自动安装脚本
::
:: jdk 15 下载地址 https://docs.aws.amazon.com/corretto/latest/corretto-15-ug/downloads-list.html
:: agent 安装包保存放在 服务器:10.16.30.9 目录: /data/qa_platform/qap-release/upload/download 中
::    按照如下步骤替换服务器 agnet.zip
::    1、解压缩 unzip agent.zip
::    2、给 jar 改名 mv qa-platform-agent-0.0.1.jar agent.jar
::    3、修改配置文件
::    4、压缩 agengt 包 zip -q -r agent.zip qap-agent/
::    5、计算 md5 并改到对应文件中保存 md5sum agent.zip
::
:start

set HOME=%cd%

echo echo off > start.bat
echo CHCP 65001 ^> null >> start.bat
echo del /q null >> start.bat
echo set HOME=%%cd%%>> start.bat

::
:: 设置对应安装包
::
set SYSTEM_INFO=当前系统 [ Windows ]
set JDK_FILE=https://corretto.aws/downloads/resources/15.0.2.7.1/amazon-corretto-15.0.2.7.1-windows-x64-jdk.zip
set AGENT_FILE=https://qa.baijiayun.com:7171/folder/upload/download/qapplatform/agent.zip
set AGENT_CHECKSUM=https://qa.baijiayun.com:7171/folder/upload/download/qapplatform/agent.checksum

::
:: 显示
::
echo ================================================================================
echo = QAplatform 测试平台 Agent 安装脚本
echo = 当前运行目录: %cd%
echo = %SYSTEM_INFO%
echo = JAVA 安装文件: %JDK_FILE%
echo = agent 安装文件: %AGENT_FILE%
echo =
echo = 注意：安装过程中会从国外网站下载对应安装文件，会有被墙后安装失败现象。尽量翻墙后再运行此脚本
echo =
echo ================================================================================

::
:: 下载、安装7-zip
::
echo 开始下载 7-zip ......
rd /s /q 7zip
mkdir 7zip
curl https://www.7-zip.org/a/7zr.exe --ssl-no-revoke --output ./7zip/7zr.exe
curl https://www.7-zip.org/a/7z2201-extra.7z --ssl-no-revoke --output ./7zip/7z2201-extra.7z
echo 7-zip 下载完成，开始安装 7-zip ......
%HOME%/7zip/7zr.exe x -y %HOME%/7zip/7z2201-extra.7z -o%HOME%/7zip/
echo 7-zip 安装完成 ......

::
:: 安装 JDK
::
set Input=n
set /p Input=是否安装 JDK？ 安装[Y]，跳过[N]
if /i '%Input%'=='y' goto yes
goto no
:yes
	echo 开始下载 JDK ......
	curl %JDK_FILE% --output jdk_15.zip
	echo 下载 JDK 完成，开始安装 JDK ......
	rd /s /q jdk15
	%HOME%/7zip/7za.exe x -y jdk_15.zip
	move jdk15* jdk15
	del /q jdk_15.zip

	:: java 环境变量
	echo :: >> start.bat
	echo :: java 环境变量 >> start.bat
	echo :: >> start.bat
	set JAVA_HOME=%HOME%/jdk15
	echo set JAVA_HOME=%%HOME%%/jdk15>> start.bat
	set PATH=%JAVA_HOME%/bin;%PATH%
	echo set PATH=%%JAVA_HOME%%/bin;%%PATH%%>> start.bat

	java -version
	echo JDK 安装完成
:no

::
:: 安装 agent，开始
::
echo 开始下载 agent ......
curl %AGENT_FILE% --output agent.zip
echo 下载 agent 完成，开始安装 agent ......"
rd /s /q agent
%HOME%/7zip/7za.exe x -y agent.zip
move qap-agent* agent
certutil -hashfile agent.zip MD5 > agent.checksum
del /q agent.zip
rd /s /q __MACOSX
echo agent 安装完成
::
:: 安装 agent，结束
::

::
:: 为了防止进入 agent 目录后找不到启动命令，在 agnet 目录中补一个启动命令
::
echo echo off > agent\start.bat
echo CHCP 65001 ^> null >> agent\start.bat
echo del /q null >> agent\start.bat
echo cd ..>> agent\start.bat
echo start.bat>> agent\start.bat

::
:: 为了启动 agent 写 start.bat，开始
::
echo echo 检查 agent 版本 ...... >> start.bat
echo for /F %%%%i in ('curl %AGENT_CHECKSUM%') do ( set CHECK_SUM=%%%%i) >> start.bat
echo echo %%CHECK_SUM%%^| findstr %%CHECK_SUM%% agent.checksum^>null ^&^& ( >> start.bat
echo 	echo agent 版本检查完毕...... >> start.bat
echo ) ^|^| ( >> start.bat
echo 	echo 开始下载 agent ...... >> start.bat
echo 	curl %AGENT_FILE% --output agent.zip >> start.bat
echo 	echo 下载 agent 完成，开始安装 agent ...... >> start.bat
echo 	%%HOME%%/7zip/7za.exe x -y agent.zip >> start.bat
echo 	move %%HOME%%\agent\config\application.yml . >> start.bat
echo 	rd /s /q agent >> start.bat
echo 	move qap-agent* agent >> start.bat
echo 	FC %HOME%\application.yml %HOME%\agent\config\application.yml ^> yml.diff >> start.bat
echo 	findstr ^"version:^" yml.diff^>null ^&^& ( >> start.bat
echo 	    del /q %HOME%\application.yml >> start.bat
echo 	    echo ================================================================================ >> start.bat
echo 	    echo = application.yml 配置文件已更新，请修改后再次启动 agent >> start.bat
echo 	    echo ================================================================================ >> start.bat
echo 	) ^|^| ( >> start.bat
echo 	    move /y application.yml agent\config\ >> start.bat
echo 	) >> start.bat
echo 	del /q %HOME%\yml.diff >> start.bat
echo 	certutil -hashfile agent.zip MD5 ^> agent.checksum >> start.bat
echo 	del /q agent.zip >> start.bat
echo 	echo agent 更新完成 >> start.bat
echo 	echo echo off ^> agent\start.bat >> start.bat
echo 	echo CHCP 65001 ^> null ^>^> agent\start.bat >> start.bat
echo 	echo del /q null ^>^> agent\start.bat >> start.bat
echo 	echo cd ..^>^> agent\start.bat >> start.bat
echo 	echo start.bat^>^> agent\start.bat >> start.bat
echo ) >> start.bat

echo :: >> start.bat
echo :: 启动 agent >> start.bat
echo :: >> start.bat
echo cd agent >> start.bat
echo java --add-opens=java.base/java.lang=ALL-UNNAMED -Dfile.encoding=utf-8 -jar qaa.jar >> start.bat
echo cd %%HOME%% >> start.bat
echo timeout /t 15 >> start.bat
echo exit >> start.bat

::
:: 为了启动 agent 写 start.bat，结束
::

echo ================================================================================
echo = 全部安装完成，请使用 ./start.bat 命令启动 agent
echo =
echo = 注意：启动 agent 前请先进入如下配置：
echo =       1、修改 agent key
echo =           - 在浏览器中访问 QAplatform 测试平台：https://qa.baijiayun.com/
echo =           - 登录后点击 设备中心 -^> 添加，输入Agent信息后点击 确定
echo =           - 在 Agent 表格中找到新增的 Agent，点击 点击复制 按钮
echo =           - 打开 .agent/config/application.yml 文件
echo =           - 找到第8行 key: xxxx-xxxx-xxxxx-xxxxx-xxxxx
echo =           - 将 xxxx-xxxx-xxxxx-xxxxx-xxxxx 替换成步骤3中复制的 key
echo =
echo ================================================================================
