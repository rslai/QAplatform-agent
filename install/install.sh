#!/bin/bash
# mac & linux 自动安装脚本
#
# jdk 15 下载地址 https://docs.aws.amazon.com/corretto/latest/corretto-15-ug/downloads-list.html
# agent 安装包保存放在 服务器:10.16.30.9 目录: /data/qa_platform/qap-release/upload/download 中
#    按照如下步骤替换服务器 agnet.zip
#    1、解压缩 unzip agent.zip
#    2、给 jar 改名 mv qa-platform-agent-0.0.1.jar agent.jar
#    3、修改配置文件
#    4、压缩 agengt 包 zip -q -r agent.zip qap-agent/
#    5、计算 md5 并改到对应文件中保存 md5sum agent.zip

HOME=$(pwd)

echo "#!/bin/bash" > start.sh
echo "HOME=\$(pwd)" >> start.sh

# 根据操作系统和cpu类型，设置对应安装包
if [ "$(uname)" = "Darwin" ]; then
	if [[ $(sysctl -n machdep.cpu.brand_string) =~ "Apple" ]]; then
		SYSTEM_INFO="当前系统: Mac OS，cpu型号: Apple"
		JDK_FILE="https://corretto.aws/downloads/resources/15.0.2.7.1/amazon-corretto-15.0.2.7.1-macosx-x64.tar.gz"
	else
		SYSTEM_INFO="当前系统: Mac OS，cpu型号: Intel"
		JDK_FILE="https://corretto.aws/downloads/resources/15.0.2.7.1/amazon-corretto-15.0.2.7.1-macosx-x64.tar.gz"
	fi
elif [ "$(expr substr $(uname -s) 1 5)" = "Linux" ]; then
	SYSTEM_INFO="当前系统 [ GNU/Linux ]"
	JDK_FILE="https://corretto.aws/downloads/resources/15.0.2.7.1/amazon-corretto-15.0.2.7.1-linux-x64.tar.gz"
fi
AGENT_FILE="https://qa.baijiayun.com:7171/folder/upload/download/qapplatform/agent.zip"
AGENT_CHECKSUM="https://qa.baijiayun.com:7171/folder/upload/download/qapplatform/agent.checksum"

# 显示
echo ================================================================================
echo = QAplatform 测试平台 Agent 安装脚本
echo = 当前运行目录: $HOME
echo = $SYSTEM_INFO
echo = JAVA 安装文件: $JDK_FILE
echo = agent 安装文件: $AGENT_FILE
echo =
echo "= \033[31m注意：安装过程中会从国外网站下载对应安装文件，会有被墙后安装失败现象。尽量翻墙后再运行此脚本\033[0m"
echo =
echo ================================================================================

# 安装 JDK
echo "是否安装 JDK？ \033[31m安装[Y]\033[0m，跳过[N]"
read input
if [ "$input" = "Y" ] || [ "$input" = "y" ]; then
	echo "开始下载 JDK ......"
	curl $JDK_FILE --output jdk15.tar.gz
	echo "下载 JDK 完成，开始安装 JDK ......"
	rm -rf jdk15
	tar -xzf jdk15.tar.gz
	mv amazon-corretto-15.* jdk15
	rm -rf jdk15.tar.gz

	# java 环境变量
	echo "# java 环境变量" >> start.sh
	if [ "$(uname)" = "Darwin" ]; then
  	export JAVA_HOME=$HOME/jdk15/Contents/Home
  	echo "export JAVA_HOME=\$HOME/jdk15/Contents/Home" >> start.sh
  elif [ "$(expr substr $(uname -s) 1 5)" = "Linux" ]; then
  	export JAVA_HOME=$HOME/jdk15
    echo "export JAVA_HOME=\$HOME/jdk15" >> start.sh
  fi
	export PATH=$JAVA_HOME/bin:$PATH
	echo "export PATH=\$JAVA_HOME/bin:\$PATH" >> start.sh

	java -version
	echo "\033[32mJDK 安装完成\033[0m"
fi

# 安装 agent，开始
echo "开始下载 agent ......"
curl $AGENT_FILE --output agent.zip
echo "下载 agent 完成，开始安装 agent ......"
rm -rf agent
unzip -q agent.zip
mv qap-agent* agent
if [ "$(uname)" = "Darwin" ]; then
  md5 agent.zip > agent.checksum
elif [ "$(expr substr $(uname -s) 1 5)" = "Linux" ]; then
  md5sum agent.zip > agent.checksum
fi
rm -rf agent.zip
chmod 777 agent
echo "\033[32magent 安装完成\033[0m"
# 安装 agent，结束

# 为了启动 agent 写 start.sh，开始
echo "echo \"停止所有 agent ......\"" >> start.sh
echo "pkill -f qaa.jar" >> start.sh
echo "echo \"检查 agent 版本 ......\"" >> start.sh
echo "REMOTE_CHECKSUM=\$(curl $AGENT_CHECKSUM)" >> start.sh
echo "LOCAL_CHECKSUM=\$(cat agent.checksum)" >> start.sh
echo "if [[ \$LOCAL_CHECKSUM =~ \$REMOTE_CHECKSUM ]]; then" >> start.sh
echo "	echo \"agent 版本检查完毕......\"" >> start.sh
echo "else" >> start.sh
echo "	echo \"开始下载 agent ......\"" >> start.sh
echo "	curl $AGENT_FILE --output agent.zip" >> start.sh
echo "	echo \"下载 agent 完成，开始安装 agent ......\"" >> start.sh
echo "	unzip -q agent.zip" >> start.sh
echo "	mv ./agent/config/application.yml ." >> start.sh
echo "	rm -rf agent" >> start.sh
echo "	mv qap-agent* agent" >> start.sh
echo "  YML_DIFF=\$(diff ./application.yml agent/config/application.yml)" >> start.sh
echo "  if [[ \$YML_DIFF =~ \"version:\" ]]" >> start.sh
echo "  then" >> start.sh
echo "    rm -f ./application.yml" >> start.sh
echo "    echo \"================================================================================\"" >> start.sh
echo "	  echo \"= \\\\033[31mapplication.yml 配置文件已更新，请修改后再次启动 agent\\\\033[0m\"" >> start.sh
echo "    echo \"================================================================================\"" >> start.sh
echo "  else" >> start.sh
echo "	  mv -f ./application.yml ./agent/config/" >> start.sh
echo "  fi" >> start.sh
if [ "$(uname)" = "Darwin" ]; then
  echo "	md5 agent.zip > agent.checksum" >> start.sh
elif [ "$(expr substr $(uname -s) 1 5)" = "Linux" ]; then
  echo "	md5sum agent.zip > agent.checksum" >> start.sh
fi
echo "	rm -rf agent.zip" >> start.sh
echo "	chmod 777 agent" >> start.sh
echo "	echo \"\\\\033[32magent 更新完成\\\\033[0m\"" >> start.sh
echo "fi" >> start.sh

echo "# 启动 agent" >> start.sh
echo "cd agent" >> start.sh
echo "echo \"启动 agent ......\"" >> start.sh
echo "nohup java --add-opens=java.base/java.lang=ALL-UNNAMED -Dfile.encoding=utf-8 -jar qaa.jar > ../log.log 2>&1 &" >> start.sh
echo "echo \"agent 启动完成，查看日志请执行 tail -f log.log\"" >> start.sh
# 为了启动 agent 写 start.sh，结束

chmod 755 start.sh

echo ================================================================================
echo "= \033[32m全部安装完成，请使用 ./start.sh 命令启动 agent\033[0m"
echo =
echo "= \033[31m注意：启动 agent 前请先进入如下配置：\033[0m"
echo "= \033[31m      1、修改 agent key\033[0m"
echo "= \033[31m          - 在浏览器中访问 QAplatform 测试平台：https://qa.baijiayun.com/\033[0m"
echo "= \033[31m          - 登录后点击 Agent 管理 -> 添加，输入Agent信息后点击 确定\033[0m"
echo "= \033[31m          - 在 Agent 表格中找到新增的 Agent，点击 点击复制 按钮\033[0m"
echo "= \033[31m          - 打开 .agent/config/application.yml 文件\033[0m"
echo "= \033[31m          - 找到第8行 key: xxxx-xxxx-xxxxx-xxxxx-xxxxx\033[0m"
echo "= \033[31m          - 将 xxxx-xxxx-xxxxx-xxxxx-xxxxx 替换成步骤3中复制的 key\033[0m"
echo = 
echo ================================================================================
